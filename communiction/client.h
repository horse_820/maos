/*
 * client.h
 *
 *  Created on: 2020年7月16日
 *      Author: MCX
 */

#ifndef CLIENT_H_
#define CLIENT_H_

#ifdef __cplusplus
extern "C"{
#endif

#include "transport.h"
#include "protocol.h"

typedef struct
{
	const char  	*driverName;		//驱动名称
	const char  	*memName;			//缓存所在动态内存的名称
	uint32_t		baud;				//驱动底层波特率
	POCOTOLFUN		*prol;				//协议
	char			*serverName;		//服务器域名
	uint16_t		serverPort;			//服务器端口
	bool			tcp;				//tcp or udp
	
	uint16_t		TDataMaxSize;		//发送数据最大长度,默认1024
	uint16_t		loopTime;			//执行周期,默认10ms
	void			*client;			//客户端(自动赋值)

		
}CLIENT_INI;



/*--------------------------------定义通讯类寄存器结构体----------------------------------*/
typedef	struct class_client	CLIENT;
typedef	struct class_client	*pCLIENT;

struct class_client
{
	//public:
	const char  				*name;								//对象名称


	/*
	****************************************************************************//********************
	*  函数: 发送函数
	*  参数: object 通讯对象
	*  参数: data 数据
	*  返回: 1：成功  0：失败
	****************************************************************************//********************
	*/	
	bool 	(*send_data)(pCLIENT object,PACKDATA *data);

	/*
	****************************************************************************//********************
	*  函数: 发送函数
	*  参数: object 通讯对象
	*  参数: data 数据
	*  返回: 1：成功  0：失败
	****************************************************************************//********************
	*/	
	bool 	(*send_ack)(pCLIENT object,PACKDATA *data);
	
	/*
	****************************************************************************//********************
	*  函数: 周期循环函数 TASK不工作时单独调用
	*  参数: object 通讯对象
	*  返回: 无
	****************************************************************************//********************
	*/
	void 	(*loop)(pCLIENT object);	
	
	/*
	****************************************************************************//********************
	*  函数: 应答处理
	*  参数: obj: 通讯对象
	*  参数: res ：应答结果
	*  返回: 无
	****************************************************************************//********************
	*/	
	void 		(*ack_handle)(void *obj,bool res);	

	/*
	****************************************************************************//********************
	*  函数: 数据缓存是否为空
	*  参数: object 通讯对象
	*  返回: 1：空 0：非空
	****************************************************************************//********************
	*/
	bool		(*is_sendBuf_empty)(pCLIENT object);

	/*
	****************************************************************************//********************
	*  函数: 获取等待应答SN
	*  参数: object 通讯对象
	*  返回: SN
	****************************************************************************//********************
	*/
	uint16_t	(*get_waitSN)(pCLIENT object);
	
	/*
	****************************************************************************//********************
	*  函数: 更改服务器
	*  参数: object 通讯对象
	*  参数: name 服务器名称
	*  参数: port 服务器端口	
	*  参数: tcp 1:tcp 	0:udp
	*  参数: prol 协议
	*  返回: 无
	****************************************************************************//********************
	*/
	void 		(*change_server)(pCLIENT object,char *name,uint16_t port,bool tcp,POCOTOLFUN *prol);

	/*
	****************************************************************************//********************
	*  函数: 更改解析函数
	*  参数: object 通讯对象
	*  参数: analyze 解析函数
	*  返回: 无
	****************************************************************************//********************
	*/
	void 		(*change_prol)(pCLIENT object,POCOTOLFUN *prol);	

	
	//protect
	void	*protect;								//内部保护变量
};
extern pCLIENT udpDebug;

/*--------------------------------定义通讯类功能结构体----------------------------------*/
typedef struct
{
	pCLIENT 	(*create)(const char *name,CLIENT_INI *arg);
	void 		(*free)(pCLIENT object);
	pCLIENT 	(*find)(const char *name);	
}TCLIENTFUN;
extern TCLIENTFUN Tclient;



#ifdef __cplusplus
}
#endif
#endif /* CLIENT_H_ */
