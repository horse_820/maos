/*
 * communication.c 
 *
 *  Created on: 2021年9月28日
 *      Author: MCX
 */

#include "comm.h"
#include "maos_queue.h"
#include "maos_malloc.h"
#include "maos_task.h"
#include "driver.h"

#define		RECVBUFSIZE		256
#define		SENDBUFSIZE		256
#define		DEFLOOPTIME		10						//单个通讯默认发送间隔 10ms
#define		LOOPTIME		5						//所有通讯周期执行时间 5ms

//----------------------定义类保护变量结构体-----------------------------//
typedef struct
{
	pCOMM			next;							//下一对象
	uint32_t		loopTick;						//周期执行时钟
	uint32_t		waitTick;						//等待应答时钟
	COMM_INI		ini;							//配置
	POTOCOLACK		*ack;							//协议应答寄存器	NULL：不需要应答
	DRIVER			*driver;						//驱动
	uint8_t			*sendBuf;						//发送缓存 
	uint8_t			sendTime;						//发送次数
	
	pQUEUE			ackQueue;						//应答发送队列	<TYPE_QUEUE>
	pQUEUE			dataQueue;						//数据发送队列	<TYPE_QUEUE>

}PROTECT;

//----------------------声名函数----------------------------------------//
static pCOMM create(const char *name,COMM_INI *arg);
static void free(pCOMM object);
static void loop(pCOMM object);
static pCOMM find(const char *name);
static void comm_loop(void);
static void loop(pCOMM object);

//----------------------建立变量----------------------------------------//
TCOMMFUN Tcomm={create,free,find};
static pCOMM firstObj=NULL;						//首个对象
static uint8_t	recvBuf[RECVBUFSIZE];			//接收缓存

/*
****************************************************************************//********************
*  函数: 应答处理
*  参数: obj: 通讯对象
*  参数: res ：应答结果
*  返回: 无
****************************************************************************//********************
*/
static void ack_handle(void *obj ,bool res)
{
	pCOMM comm = obj;														//指向通讯对象
	PROTECT *protect =comm->protect;										//指向私有变量
	PACKDATA *packData = (PACKDATA *)protect->sendBuf;						//指向发送数据包
	//参数确认
	if(!obj || !protect || !protect->ack ||!packData)
		return;	

	
	if(!res)																//应答错误
	{
		if(protect->sendTime<protect->ack->sendMaxTime)						//还可以重发
		{
			protect->ack = NULL;											//结束本次等待应答
			return;															//返回执行重发
		}
	}

	//协议层应答处理
	if(packData->ack.back)
		packData->ack.back(packData->ack.arg,res);
	
	//初始等待应答数据
	protect->sendTime=0;													//初始发送次数
	packData->dataLen=0;													//初始发送数据长度
	protect->ack = NULL;													//结束等待应答
	

}

/*
****************************************************************************//********************
*  函数: 发送应答 
*  参数: object 通讯对象
*  参数: package ：数据包
*  返回: 1：成功   0：失败
****************************************************************************//********************
*/
static bool	write_ack(pCOMM object,PACKDATA *package)
{
	//参数确认
	uint16_t len = PACKAGESIZE(package->dataLen);
	if(!object || !package || !len )
		return false;
	PROTECT *protect =object->protect;										//指向私有变量
	if(!protect)																											
		return false;
	
	

	bool res =  false;
	pQUEUE queue = protect->ackQueue;										//指向应答队列
	res = queue->push(queue,package,len);									//写入队列
	
	protect->loopTick = maos_tick-protect->ini.loopTime;					//下一周期（5ms）立马执行发送
	return res;																//返回结果
	
}

/*
****************************************************************************//********************
*  函数: 发送数据 
*  参数: object 通讯对象
*  参数: package ：数据包
*  返回: 1：成功   0：失败
****************************************************************************//********************
*/
static bool	write_data(pCOMM object,PACKDATA *package)
{
	//参数确认
	uint16_t len = PACKAGESIZE(package->dataLen);
	if(!object || !package || !len )
		return false;
	
	PROTECT *protect =object->protect;										//指向私有变量
	if(!protect)																											
		return false;
	
	bool res =  false;
	pQUEUE queue = protect->dataQueue;										//指向数据队列
	res = queue->push(queue,package,len);									//写入队列
	return res;																//返回失败
}

/*
****************************************************************************//********************
*  函数: 周期函数
*  参数: object 通讯对象
*  参数: 无
*  返回: 无
****************************************************************************//********************
*/
static void loop(pCOMM object)
{
	PROTECT *protect =object->protect;										//指向私有变量
	if(!object || !protect || !protect->driver )							//确认参数
		return;
	CHAR_DEVICE_OPS *ops = protect->driver->ops.charDevice;					//驱动操作集
	if(!ops->pfn_read || !ops->pfn_write)									//确认读写函数
		return;
	
	//周期执行
	if(	maos_tick - protect->loopTick < protect->ini.loopTime)
		return;
	protect->loopTick =maos_tick;											//同步时钟	
	
	uint16_t len=0;
	PACKDATA *packData = (PACKDATA *)protect->sendBuf;						//指向发送数据包
	ANALYZE analyze = protect->ini.prol->analyze;							//指向解析函数

	//-------------------------------------接收处理--------------------------------------//
	if(analyze)																//需要解析
	{
		len=ops->pfn_read(recvBuf);								//读取一条接收数据
		if(len)
			analyze(object,recvBuf,len);									//解析数据
	}
	
	//-------------------------------------发送处理--------------------------------------//
	//等待应答
	if(protect->ack)														
	{
		if(maos_tick - protect->waitTick< protect->ack->waitAckMSec)		//等待应答未超时
			return;															//返回
		protect->waitTick = maos_tick;										//同步时钟
		ack_handle(object,false);											//应答超时处理
		return;																//返回
	}

	
	//重发数据
senddata:
	if(packData->dataLen)											
	{

		bool res=ops->pfn_write(&packData->data,packData->dataLen);			//驱动层发送
		if(res)																//发送成功
		{
			protect->sendTime++;											//更新发送次数
			if(packData->ack.waitAckMSec)									//需要应答
			{
				protect->ack = &packData->ack;								//赋值应答寄存器
				protect->waitTick = maos_tick;								//同步时钟
			}
			else															//不需要应答
			{
				protect->ack = NULL;										//不等待应答
				packData->dataLen = 0;										//结束重发
			}
			
			
			protect->loopTick =maos_tick;									//发送过程占用时间，同步时钟	
		}
		return;																//返回
	}
	
	//发送应答数据	
	len = protect->ackQueue->pop(protect->ackQueue,packData);				//下载一条应答数据
	if(len && len == PACKAGESIZE(packData->dataLen))						//下载数据成功
	{
		protect->ack = NULL;												//赋值应答寄存器
		protect->sendTime = 0;												//重置发送次数
		goto senddata;														//发送数据
	}	
	
	//发送数据 
	len = protect->dataQueue->pop(protect->dataQueue,packData);				//下载一条数据
	if(len && len == PACKAGESIZE(packData->dataLen))						//下载数据成功
	{
		protect->ack = NULL;												//赋值应答寄存器
		protect->sendTime = 0;												//重置发送次数
		goto senddata;														//发送数据
	}

}

/*
****************************************************************************//********************
*  函数: 数据缓存是否为空
*  参数: object 通讯对象
*  返回: 1：空 0：非空
****************************************************************************//********************
*/
static bool is_sendBuf_empty(pCOMM object)
{
	//参数确认
	if(!object )
		return false;
	PROTECT *protect =object->protect;										//指向私有变量
	if(!protect)																											
		return false;
	pQUEUE queue = protect->dataQueue;
	if(queue->is_empyt(queue))												//数据队列为空
		return true;
	else
		return false;
}

/*
****************************************************************************//********************
*  函数: 获取等待应答SN
*  参数: object 通讯对象
*  返回: SN
****************************************************************************//********************
*/
static uint16_t	get_waitSN(pCOMM object)
{
	//参数确认
	PROTECT *protect =object->protect;										//指向私有变量
	if(!object || !protect )												//确认参数
		return 0;

	if(!protect->ack )														//不在等待应答中
		return 0;
	
	return	protect->ack->sn;
}

/*
****************************************************************************//********************
*  函数: 更改解析函数
*  参数: object 通讯对象
*  参数: analyze 解析函数
*  返回: 无
****************************************************************************//********************
*/
static void change_prol(pCOMM object,POCOTOLFUN *prol)
{
	//参数确认
	PROTECT *protect =object->protect;										//指向私有变量
	if(!object || !protect )												//确认参数
		return ;
	protect->ini.prol->disable = true;										//停止原协议
	protect->driver->ops.charDevice->pfn_close();							//关闭驱动
	protect->ini.prol = prol;												//更新协议
	protect->driver->ops.charDevice->pfn_open(&protect->ini);				//打开驱动
}

/*
****************************************************************************//********************
*  函数: 更改通讯波特率
*  参数: object 通讯对象
*  参数: baud 波特率
*  返回: 无
****************************************************************************//********************
*/
static void change_baud(pCOMM object,u32 baud)
{
	//参数确认
	PROTECT *protect =object->protect;										//指向私有变量
	if(!object || !protect )												//确认参数
		return ;
	protect->driver->ops.charDevice->pfn_close();							//关闭驱动
	protect->ini.baud = baud;												//更新波特率
	protect->driver->ops.charDevice->pfn_open(&protect->ini);				//打开驱动
}

/*------------------------------类功能实现函数----------------------------------*/
/*
****************************************************************************//********************
*  函数: 类创建函数
*  参数: name 端口名称
*  参数: ini 配置
*  返回: 对象
****************************************************************************//********************
*/
static pCOMM create(const char *name,COMM_INI *ini)
{
	//确认参数
	if(!name || !ini)
		return NULL;
	//防重处理
	pCOMM oldComm = find(name);
	free(oldComm);
		
	//申请缓存
	pMALLOC_OPS var = Tmalloc.find("MALLOC_VAR");							//查找动态内存
	if(!var)																//动态内存无效
		return NULL;														//返回空

		//处理未赋值配置
	if(ini->TDataMaxSize==0)												//未赋值 发送数据最大长度
		ini->TDataMaxSize = SENDBUFSIZE;
	if(ini->loopTime==0)													//未赋值 执行周期
		ini->loopTime = DEFLOOPTIME;
	
	pQUEUE ackQueue = Tqueue.create("omm-ack",ini->memName);				//创建发送应答队列
	pQUEUE dataQueue = Tqueue.create("comm-data",ini->memName);				//创建发送数据队列 	
	uint8_t	*sendBuf =  var->malloc(ini->TDataMaxSize);						//申请发送缓存内存
	pCOMM object = var->malloc(sizeof(cCOMM));								//申请对象公共变量内存
	PROTECT *protect = var->malloc(sizeof(PROTECT));						//申请对象私用变量内存
	DRIVER *driver = Tdriver.rigister(ini->driverName);						//注册驱动
	if(!sendBuf  || !ackQueue || !dataQueue || !protect || !object || !driver)	//内存申请失败
	{
		Tqueue.destory(ackQueue);											//释放应答队列
		Tqueue.destory(dataQueue);											//释放数据队列
		var->free(sendBuf);													//释放发送缓存
		var->free(object);													//释放对象私用变量内存																	
		var->free(protect);													//释放对象公共变量内存
		Tdriver.unregister(ini->driverName);								//卸载驱动											
		return NULL;														//返回空指针
	}
	memset(sendBuf,0,sizeof(PACKDATA));
	memset(protect,0,sizeof(PROTECT));										//初始化对象私用变量
	memset(object,0,sizeof(cCOMM));											//初始化对象
	

	ini->comm = object;														//绑定端口	
	
	//赋值私有变量
	protect->ackQueue = ackQueue;											//保存应答队列
	protect->dataQueue = dataQueue;											//保存数据队列
	protect->sendBuf = sendBuf;												//保存发送缓存
	protect->ini=*ini;														//保存对象配置
	protect->driver = driver;												//保存驱动 	
	
	//公共变量
	object->name = name;													//绑定名称
	object->send_data = write_data;											//绑定发送数据函数
	object->send_ack = write_ack;											//绑定发送应答函数
	object->loop = loop;													//绑定周期函数
	object->ack_handle = ack_handle;										//绑定应答处理函数
	object->is_sendBuf_empty = is_sendBuf_empty;							//绑定函数
	object->get_waitSN = get_waitSN;										//绑定函数
	object->change_prol = change_prol;										//绑定函数
	object->change_baud = change_baud;										//绑定函数
	object->protect =protect; 												//绑定私用变量
	

	//打开驱动 
	if(protect->driver->ops.charDevice->pfn_open)							//驱动需要打开
		protect->driver->ops.charDevice->pfn_open(ini);						//打开驱动
	
	//对象链表处理
	pCOMM obj = firstObj;													//指向首对象
	if(!obj)																//无首对象
	{
		firstObj = object;													//赋值首对象
		Task.pfn_add(comm_loop,TASK_COMMON,LOOPTIME);						//添加周期任务5ms
	}
	else																	//之前有对象
	{
		while(obj && obj->protect)											//匹配所有对象
		{
			protect = obj->protect;											//指向私有变量
			if(protect->next)												//本节点非尾节点
				obj = protect->next;										//下一节点
			else															//本节点为尾节点
			{
				protect->next = object;										//增加本对象
				break;														//结束匹配
			}
		}
	}

	return object;															//返回对象
}

/*
****************************************************************************//********************
*  函数: 类释放函数
*  参数: object ：对象
*  返回: 无
****************************************************************************//********************
*/
static void free(cCOMM* object)
{
	PROTECT *pt;
	PROTECT *protect = object->protect;										//指向相应的保护变量
	//参数确认
	if(!object || !protect)
		return ;
	

	//对象链表删除本对象
	pCOMM obj = firstObj;													//指向首对象
	if(obj==object)															//本对象为首对象
		firstObj =  protect->next;											//首对象指向本对象下节点
	else																	//本对象非首对象
	{
		while(obj && obj->protect)											//匹配所有对象
		{
			pt = obj->protect;												//指向私有变量
			if(pt->next == object)											//匹配到对象上节点
			{
				pt->next =protect->next;									//对象上节点的下节点 为 对象下节点
				break;														//结束匹配
			}
			obj = pt->next;													//指向下一节点
		}
	}

	//动态内存
	pMALLOC_OPS var = Tmalloc.find("MALLOC_VAR");							//查找动态内存
	if(!var)																//动态内存无效
		return;																//返回

	//释放内存
	Tqueue.destory(protect->ackQueue);										//释放应答队列
	Tqueue.destory(protect->dataQueue);										//释放数据队列
	var->free(protect->sendBuf);											//释放发送缓存
	Tdriver.unregister(protect->ini.driverName);							//卸载驱动	
	var->free(protect);														//释放对象公共变量内存
	var->free(object);														//释放对象私用变量内存	
	object->protect = NULL;													//对象无效
	
	if(!firstObj)
		Task.pfn_del(comm_loop);											//删除周期任务
}

/*
****************************************************************************//********************
*  函数: 类匹配函数
*  参数: name 端口名称
*  返回: 类
****************************************************************************//********************
*/
static pCOMM find(const char *name)
{
	PROTECT *protect;
	
	pCOMM obj = firstObj;													//指向首对象
	while(obj)																//匹配所有对象
	{
		if(strcmp(name,obj->name)==0)										//匹配到端口名称
			return obj;
		protect = obj->protect;												//指向相应的保护变量
		obj = protect->next;												//指向下一对象
	}
	return NULL;
}

/*
****************************************************************************//********************
*  函数: 所有通讯端口周期循环函数
*  参数: 无
*  返回: 无
****************************************************************************//********************
*/
void comm_loop(void)
{
	PROTECT *protect;
	
	pCOMM obj = firstObj;													//指向首对象
	while(obj)																//匹配所有对象
	{
		loop(obj);															//执行周期函数
		protect = obj->protect;												//指向相应的保护变量
		obj = protect->next;												//指向下一对象
	}
	
}

//end of file.
