/*
 * maos_mail.h
 *
 *  Created on: 2020��6��11��
 *      Author: MCX
 */

#ifndef MAOS_MAIL_H_
#define MAOS_MAIL_H_

#ifdef __cplusplus
extern "C"{
#endif

#include "maos_include.h" 

typedef bool (*FMAIL)(uint8_t,char *);

typedef struct class_check
{
	void    (*recv)(char *str);
	uint8_t	(*add)(char *name,uint32_t time,FMAIL back);
	void	(*del)(uint8_t handle);
	void    (*clear)(void);

}MAILFUN;
extern MAILFUN Tmail;


#ifdef __cplusplus
}
#endif
#endif /* MAOS_MAIL_H_ */
