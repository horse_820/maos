#ifndef __TREE_H__
#define __TREE_H__

#include "maos_include.h"
#include "maos_malloc.h"

#ifdef __cplusplus
extern "C"{
#endif

//***************************************定义树节点结构体*************************
typedef		struct	_treeNode		TREENODE;
typedef		struct	_treeNode		*pTREENODE;
struct _treeNode{

	pTREENODE		left;
	pTREENODE		right;
	void			*data;
};

//***************************************定义树结构体*************************

typedef		struct	_treeObj		TREE;
typedef		struct	_treeObj		*pTREE;
struct _treeObj{
	//public:
	pMALLOC_OPS		mem;						//动态内存
	pTREENODE		rootNode;
	
	pTREENODE		(*addNode)(pTREE tree,pTREENODE parent,void *data);
	void			(*delNode)(pTREE tree,pTREENODE node);
	void			(*traversal)(pTREE tree,pTREENODE node,HANDLE handle,void *arg);
	pTREENODE		(*find_parent)(pTREE tree,pTREENODE node);
	bool			(*IsNodeOnTree)(pTREE tree,pTREENODE data);
	//protect
	void	*protect;								//内部保护变量
};



typedef struct
{
	/*
	**************************************************************************************************
	*  函数: 创建树
	*  参数: name ：树名称
	*  参数: malloc ：动态内存名称
	*  参数: dataLen ：节点数据长度
	*  返回: 队列
	**************************************************************************************************
	*/
	pTREE (*create)(const char *name,const char *malloc,uint32_t dataLen);
	
	/*
	**************************************************************************************************
	*  函数: 销毁树
	*  参数: obj ：树
	*  返回: 无
	**************************************************************************************************
	*/	
	void	(*destory)(pTREE obj);
	
	/*
	**************************************************************************************************
	*  函数: 查找树
	*  参数: name ：树名称
	*  返回: 队列
	**************************************************************************************************
	*/	
	pTREE	(*find)(const char *name);	
	
	
}TTREEFUN;

extern TTREEFUN	Ttree;

#ifdef __cplusplus
}
#endif

#endif
