#ifndef _MAOS_TASK_H
#define _MAOS_TASK_H

#ifdef __cplusplus
extern "C"{
#endif

#include "maos_include.h"




typedef enum
{
	TASK_IMPORTANT=0,
	TASK_COMMON,
	TASK_FREE,
}TASKPRIORITY;

typedef void (*PFN_TASK)(void );

typedef struct
{
	uint32_t		tick;							//时戳
	void			(*pfn_loop)(void);				//循环函数
	bool			(*pfn_add)(PFN_TASK task,TASKPRIORITY priority, uint32_t	looptime);	//增加任务
	bool			(*pfn_del)(PFN_TASK task);												//删除任务
	void			(*pfn_exe)(PFN_TASK task,uint32_t looptime);							//执行任务
}TASKFUN;
extern TASKFUN Task;

/*
**************************************************************************************************
*  函数: Task模块初始化
*  参数: 无
*  返回: 无
**************************************************************************************************
*/
void task_init(void);

#ifdef __cplusplus
}
#endif
#endif
