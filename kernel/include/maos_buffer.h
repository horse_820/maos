#ifndef __BUFFER_H__
#define __BUFFER_H__

#include "maos_include.h"

#ifdef __cplusplus
extern "C"{
#endif

typedef		struct	_bufferObj		BUFFER;
typedef		struct	_bufferObj		*pBUFFER;

struct _bufferObj{

	//public:
	/*
	**************************************************************************************************
	*  函数: 匹配数据
	*  参数: obj ：缓存对象
	*  参数: data ：写入数据
	*  返回: 1：成功		0：失败
	**************************************************************************************************
	*/
	bool (*match)(BUFFER *obj,void *data,u16 len);

	/*
	**************************************************************************************************
	*  函数: 清缓存
	*  参数: obj ：缓存对象
	*  返回: 无
	**************************************************************************************************
	*/
	void (*clear)(BUFFER *obj);
	
	/*
	**************************************************************************************************
	*  函数: 写一条缓存函数
	*  参数: obj ：缓存对象
	*  参数: data ：写入数据
	*  返回: 1：成功		0：失败
	**************************************************************************************************
	*/
	bool (*push)(BUFFER *obj,void *data,u16 len);
	
	/*
	**************************************************************************************************
	*  函数: 读一条缓存函数
	*  参数: obj ：缓存对象
	*  参数: data ：返回数据
	*  返回: 数据长度
	**************************************************************************************************
	*/
	u16 (*pop)(BUFFER *obj,void *data);
	
	//protect
	void	*protect;								//内部保护变量
};


typedef struct
{
	/*
	**************************************************************************************************
	*  函数: 创建缓存
	*  参数: name ：缓存名称
	*  参数: malloc ：动态内存名称
	*  参数: size ：一条缓存大小
	*  参数: totalnum ：缓存条数
	*  返回: 缓存
	**************************************************************************************************
	*/
	pBUFFER (*create)(const char *name,const char *malloc, uint16_t size,uint16_t totalnum);
	
	/*
	**************************************************************************************************
	*  函数: 销毁缓存
	*  参数: obj ：缓存对象
	*  返回: 无
	**************************************************************************************************
	*/	
	void	(*destory)(pBUFFER obj);
	
	/*
	**************************************************************************************************
	*  函数: 查找缓存
	*  参数: name ：缓存缓存名称
	*  返回: 缓存
	**************************************************************************************************
	*/	
	pBUFFER	(*find)(const char *name);	
	
	
}BUFFERFUN;

extern BUFFERFUN	Tbuffer;

#ifdef __cplusplus
}
#endif

#endif
