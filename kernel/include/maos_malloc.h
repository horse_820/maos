#ifndef _MAOS_MALLOC_H
#define _MAOS_MALLOC_H

#ifdef __cplusplus
extern "C"{
#endif

#include "maos_malloc_bsp.h"

/*--------------------------------定义类数据结构体----------------------------------*/
typedef struct _malloc_ops		MALLOC_OPS;
typedef struct _malloc_ops		*pMALLOC_OPS;

struct _malloc_ops
{
	void*			(*malloc)(u32 size);						//申请内存
	void			(*free)(void *data);						//释放内存
	void*			(*remalloc)(void *data,u32 size);			//t重新申请内存
	u32				(*get_residue)(void);						//剩余内存
};


typedef struct 
{
	const char			*name;							//名称
	pMALLOC_OPS			ops;							//操作集合
	FINIT				init;							//入口函数
	FINIT				exit;							//出口函数
}MALLOCREG;

#define MALLOC_RIGISTER(__name,_name,_ops,_init,_exit)		\
	MALLOCREG	malloc_##__name __attribute__((section("mallocTable"))) = \
	{\
		.name = _name,\
		.ops = _ops,\
		.init = _init,\
		.exit = _exit\
	}

//------------------定义类功能结构体------------------//
typedef struct
{
	/*
	**************************************************
	*  函数: 注册对象
	*  参数: name 对象名称
	*  返回: 对象
	**************************************************
	*/
	pMALLOC_OPS (*rigister)(const char *name);

	
	/*
	**************************************************
	*  函数: 注销对象
	*  参数: name 对象名称
	*  返回: 无
	**************************************************
	*/
	void (*unregister)(const char *name);

	/*
	**************************************************
	*  函数: 查找设备
	*  参数: 参数: name 对名称
	*  返回: 对
	**************************************************
	*/
	pMALLOC_OPS (*find)(const char *name);

}TMALLOCFUN;
extern TMALLOCFUN  Tmalloc;					//对象操作函数

void malloc_init(void);

#ifdef __cplusplus
}
#endif

#endif
