
#ifndef _MAOS_MALLOC_BSP_H
#define _MAOS_MALLOC_BSP_H

#ifdef __cplusplus
extern "C"{
#endif

#include "maos_include.h"

//------------定义内存块寄存器变量结构体------------------//
typedef struct
{
	u32				poolBase;						//内存池首
	u32				poolEnd;						//内存池尾
	u32				minDataSize;					//内存块最小数据大小
	bool			enable;							//使能标志
}MALLOCPOOL;

void *maos_malloc(MALLOCPOOL* pool,u32 size);
void *maos_remalloc(MALLOCPOOL* pool,void *data,u32 size);
void maos_free(MALLOCPOOL* pool,void *data);
void maos_malloc_init(MALLOCPOOL* pool);
u32 maos_malloc_residue(MALLOCPOOL* pool);

extern volatile uint32_t maos_tick;
#ifdef __cplusplus
}
#endif

#endif
