/*
 * mainclude.h
 *
 *  Created on: 2021��1��20��
 *      Author: MA
 */

#ifndef _MAINCLUDE_H_
#define _MAINCLUDE_H_

#ifdef __cplusplus
extern "C"{
#endif

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <limits.h>
#include "stm32f10x.h"

//#include "buffer/maos_buffer.h"
//#include "buffer/maos_queue.h"
//#include "system/maos_sys.h"
//#include "malloc/maos_malloc.h"
//#include "task/maos_task.h"

typedef uint32_t  u32;
typedef uint16_t u16;
typedef uint8_t  u8;
 
typedef volatile uint32_t  vu32;
typedef volatile uint16_t vu16;
typedef volatile uint8_t  vu8; 

typedef void* (*DRIVER_INIT)(void)	;
typedef void (*DRIVER_EXIT)(void)	;
typedef void (*FLOOP)(void)	;
typedef void (*FINIT)(void)	;
typedef void (*FRECV)(void *,void *, uint16_t);
typedef void (*ANALYZE)(void *,uint8_t *, uint16_t);
typedef bool (*HANDLE)(void *,void *);

extern volatile uint32_t maos_tick;



#ifdef __cplusplus
}
#endif

#endif /* _MAINCLUDE_H_ */

