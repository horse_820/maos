/*
 * maos_task.c
 *
 *  Created on: 2021年1月23日
 *      Author: mcx
 *---------------V1.1 20210928  by mcx----------------- 
 1. 增加 void exe(PFN_TASK task,uint32_t	delayms) 函数
 2.	去掉 bool del(PFN_TASK task) 函数参数 TASKPRIORITY priority
 
 */
 
#include "maos_task.h"
#include "maos_sys.h"

/*---------------------宏定义------------------------------------*/
#define   		TASKPRONUM			3				//任务优先级别
#define   		TASKMAXNUM			32				//单级别任务数
#define			TASKFLAG			0x5441534B		//'TASK'

/*-----------------------定义任务数据结构-----------------------*/
typedef struct
{
	uint32_t		flag;							//任务有效标志
	uint32_t		tick;							//时戳
	uint32_t		looptime;						//循环周期（ms)	0:立即执行
	void			(*pfn_task)(void);				//任务函数
	uint8_t			priority;						//优先级 0最高
	bool			lock;							//任务执行中标志
}TASK;

/*-----------------------定义任务列表数据结构-------------------*/
typedef struct
{
	TASK			task[TASKMAXNUM];				//任务列表
	uint8_t			num;							//任务总数
	uint8_t			index;							//任务序号
}TASKLIST;

/*-----------------------定义变量--------------------------------*/
TASKFUN Task;										//任务功能函数
static TASKLIST TaskList[TASKPRONUM];				//定义任务列表

/*
**************************************************************************************************
*  函数: 执行重要任务函数 第次执行所有任务
*  参数: list：任务列表
*  返回: 无
**************************************************************************************************
*/
static void important_task_exe(TASKLIST *list)
{
	uint8_t i;
	TASK    *task;
	
	if(list->num == 0)
		return;
	
	for(i=0;i<TASKMAXNUM;i++)
	{
		if(list->task[i].flag == TASKFLAG )										//任务
		{
			task = &list->task[i];												//取出任务
			//判断计时器
			if(!task->lock && Task.tick - task->tick >= task->looptime)			//定时到
			{
				task->tick =	Task.tick;										//同步时钟
				list->index = i;												//任务序号
				task->lock = true;												//任务上锁
				task->pfn_task();												//执行任务
				task->lock = false;												//任务解锁
			}
		}
	}
}	

/*
**************************************************************************************************
*  函数: 执行一般任务函数 每次只执行一个任务
*  参数: list：任务列表
*  返回: 无
**************************************************************************************************
*/
static bool common_task_exe(TASKLIST *list)
{
	uint8_t i;
	TASK    *task;
	bool res = false;
	
	if(list->num == 0)
		return res;
	
	for(i=0;i<TASKMAXNUM;i++)
	{
		//下一任务序号
		if(++list->index>=TASKMAXNUM)
			list->index = 0;
		if(list->task[list->index].flag == TASKFLAG)						//任务
		{
			task = &list->task[list->index];								//取出任务
			//判断计时器
			if(!task->lock && Task.tick - task->tick >= task->looptime)		//定时到
			{
				task->tick = Task.tick;										//同步时钟
				task->lock = true;											//任务上锁
				task->pfn_task();											//执行任务
				task->lock = false;											//任务解锁
				res = true;
				break;
			}
		}
	}
	return res;
}

/*
**************************************************************************************************
*  函数: 增加任务环函数
*  参数: pfn_task：任务函数
*  参数: para：任务传递参数
*  参数: priority：任务优先级
*  参数: looptime：循环周期
*  返回: 1：成功			0：失败
**************************************************************************************************
*/
static bool add(void	(*pfn_task)(void),TASKPRIORITY	priority, uint32_t	looptime)
{
	TASKLIST *list;
	uint8_t i;
	
	if(priority >=TASKPRONUM)
		return false;
	
	list = &TaskList[priority];												//赋值列表
	if(list->num>=TASKMAXNUM)
		return false;														//任务满
	//防重处理
	for(i=0;i<TASKMAXNUM;i++)
	{
		if(list->task[i].flag != TASKFLAG )									//空任务
			continue;
		if(list->task[i].pfn_task == pfn_task)								//任务已添加
			return false;
	}
	
	//寻找空任务
	for(i=0;i<TASKMAXNUM;i++)
	{
		if(list->task[i].flag != TASKFLAG)									//空任务
		{
			//赋值任务参数
			list->task[i].flag = TASKFLAG;									//赋值有效任务标志
			list->task[i].looptime = looptime;								//保存执行周期
			list->task[i].pfn_task = pfn_task;								//绑定执行函数
			list->task[i].priority = priority;								//保存优先级
			list->task[i].lock = false;										//初始解锁状态
			list->task[i].tick = Task.tick;									//初始时戳
			list->num++;													//列表内任务总数增加
			break;
		}
	}
	return true;
}

/*
**************************************************************************************************
*  函数: 删除任务环函数
*  参数: task：任务函数
*  返回: 1：成功			0：失败
**************************************************************************************************
*/
static bool del(PFN_TASK task)
{
	TASKLIST *list;
	uint8_t i;
	TASKPRIORITY priority;
	
	for(priority = TASK_IMPORTANT;priority<=TASK_FREE;priority++)
	{
		list = &TaskList[priority];											//赋值列表
		if(list->num==0)
			continue;														//无任务
		
		//匹配任务
		for(i=0;i<TASKMAXNUM;i++)
		{
			if(list->task[i].flag == TASKFLAG && list->task[i].pfn_task == task)		//匹配到任务
			{
				memset(&list->task[i],0,sizeof(TASK));									//清除任务
				list->num--;															//列表内任务总数增加
				return true;
			}
		}		
	}

	return false;
}

/*
****************************************************************************//********************
*  函数: 执行函数
*  参数: task：任务函数
*  参数: delayms ms后执行
*  返回: 1：成功			0：失败
****************************************************************************//********************
*/
static void exe(PFN_TASK task,uint32_t	delayms)
{
	TASKLIST *list;
	uint8_t i;
	TASKPRIORITY priority;
	
	for(priority = TASK_IMPORTANT;priority<=TASK_FREE;priority++)
	{
		list = &TaskList[priority];											//赋值列表
		if(list->num==0)
			continue;														//无任务
		
		//匹配任务
		for(i=0;i<TASKMAXNUM;i++)
		{
			if(list->task[i].flag == TASKFLAG && list->task[i].pfn_task == task)		//匹配到任务
			{
				list->task[i].tick = Task.tick -delayms;					//调整计数器
				return;
			}
		}		
	}
}
/*
**************************************************************************************************
*  函数: 任务列表循环函数
*  参数: 无
*  返回: 无
**************************************************************************************************
*/
static void loop(void)
{
	Task.tick = maos_tick;														//同步时钟
	important_task_exe(&TaskList[0]);											//执行一次重要任务

	if(!common_task_exe(&TaskList[1]))											//执行一次一般任务
		common_task_exe(&TaskList[2]);											//执行一次空闲任务
}


/*
**************************************************************************************************
*  函数: 任务列表初始化
*  参数: list：任务列表
*  返回: 无
**************************************************************************************************
*/
static void task_list_init(TASKLIST *list)
{
	uint8_t i;
	
	list->index = TASKMAXNUM-1;							//指向最后一个任务，方便从0序号任务开始执行
	list->num = 0;
	for(i=0;i<TASKMAXNUM;i++)
		memset(&list->task[i],0,sizeof(TASK));			//清除任务

}

/*
**************************************************************************************************
*  函数: 任务模块初始化
*  参数: 无
*  返回: 无
**************************************************************************************************
*/
void task_init(void)
{
	//初始化任务功能函数
	Task.pfn_add = add;
	Task.pfn_del = del;
	Task.pfn_exe = exe;
	Task.pfn_loop = loop;
	Task.tick = 0;
	
	//初始任务列表
	uint8_t i=0;
	for(i=0;i<TASKPRONUM;i++)
		task_list_init(&TaskList[i]);

}

//end of file.
