/*
 * maos_malloc_bsp.c
 *
 *  Created on: 2021年1月20日
 *      Author: mcx
 */
#include "maos_malloc_bsp.h"
#include "maos_sys.h"

//**************************定义内存块头数据结构****************************//********************
typedef		struct _block	BLOCKHEADER;
typedef		struct _block	*PBLOCKHEADER;
struct _block {
	u32								state;									//标志
	u32								dataLen;								//data域的长度
	PBLOCKHEADER					next;									//该内存段的后继
	PBLOCKHEADER					prev; 									//该内存段的前驱
};



#define		STATE_UNUSED			0xAABB0055									//块空闲状态
#define		STATE_USED				0xAABB0155									//块使用状态
#define		BLOCKHEADER_SIZE		sizeof(BLOCKHEADER)						//块头大小

/*
****************************************************************************//********************
*  函数: 从内存池中统计空内存
*  参数: pool:内存池
*  返回: 空内存总大小
****************************************************************************//********************
*/
u32 maos_malloc_residue(MALLOCPOOL* pool)
{
	PBLOCKHEADER block = (PBLOCKHEADER)pool->poolBase;						//指向首内存块
	u32 num=0;
	while(block)															//历遍内存池中所有内存块
	{
		if(block->state==STATE_UNUSED)												//尾块
			num+=block->dataLen;
		block = block->next;												//指向下一块
	}
	return num;
}
/*
****************************************************************************//********************
*  函数: 从内存池中分配内存
*  参数: pool:内存池
*  参数: size:内存大小
*  返回: 内存
****************************************************************************//********************
*/
void *maos_malloc(MALLOCPOOL* pool,u32 size)
{
	//确认参数
	if(!pool || !size )
		return NULL;

	//计算所需要的数据长度
	u32	num = (size-1) / pool->minDataSize + 1;								//所需内存块数量 
	u32 len = num * pool->minDataSize;										//所需内存数据长度

	//从空闲的块中找出合适的块
	maos_disableInt();														//禁中断
	PBLOCKHEADER block = (PBLOCKHEADER)pool->poolBase;						//指向首内存块
	PBLOCKHEADER match = NULL;												//定义匹配到的块
	while(block)															//历遍内存池中所有内存块
	{
		if(block->state == STATE_UNUSED && block->dataLen >= len)			//满足条件的内存块
		{
			if(!match)														//之前无满足的块
				match = block;												//匹配本块
			else if(match->dataLen > block->dataLen)						//本块比之前匹配的块更合适
				match = block;												//匹配本块
			if(match->dataLen == len)										//匹配的块完美
				break;														//结束匹配
		}
		block = block->next;												//指向下一块
	}
	if(!match)																//没有匹配到合适的块
	{
		maos_enableInt();													//开中断
		return NULL;														//返回
 	}
	
	
	//分割匹配到的内存块
	if(match->dataLen - len > (BLOCKHEADER_SIZE + pool->minDataSize))		//匹配的块可以再分割出一块空闲块
	{
		u32 size = BLOCKHEADER_SIZE + len;									//计算匹配块所占有效空间大小
		PBLOCKHEADER next = (PBLOCKHEADER)((u32)match + size);				//指向分割块2
		next->dataLen = match->dataLen -size;								//赋值分割块2数据长度
		next->next = match->next;											//赋值分割块2下块
		next->prev = match;													//赋值分割块2上块
		next->state = STATE_UNUSED;											//赋值分割块2状态空闲

		if(match->next)														//分割块1有下块
			match->next->prev = next;										//下块的上块为分割块2
		match->dataLen = len;												//赋值分割块1数据长度
		match->next = next;													//赋值分割块1下块
	}
	match->state = STATE_USED;												//赋值匹配块状态使用中
	
	maos_enableInt();														//开中断
	
	return (void*)((u32)match + BLOCKHEADER_SIZE);							//返回匹配内存地址
}

/*
****************************************************************************//********************
*  函数: 合并两内存块
*  参数: block1:内存池1
*  参数: block2:内存池2
*  返回: 无
****************************************************************************//********************
*/
static void fusion_block(PBLOCKHEADER block1,PBLOCKHEADER block2)
{
	//确认参数
	if(!block1 || !block2)
		return;
	//确认块空闲
	if(block1->state != STATE_UNUSED || block2->state != STATE_UNUSED)
		return;
	//确认块物理地址相邻
	if(block1->next != block2 || block2->prev != block1)
		return;

	block1->next = block2->next;											//更新下块
	if(block2->next)
		block2->next->prev = block1;											//被删除块下块的上块
	
	block1->dataLen += block2->dataLen + BLOCKHEADER_SIZE;					//更新数据长度
	block2->state = 0;														//更新被合并块状态无效
}

/*
****************************************************************************//********************
*  函数: 从内存池中释放内存
*  参数: pool:内存池
*  参数: data:内存数据
*  返回: 无
****************************************************************************//********************
*/
void maos_free(MALLOCPOOL* pool,void *data)
{
	//确认参数
	if(!pool || !data )
		return ;
	if((u32)data<=pool->poolBase || (u32)data>=pool->poolEnd)
		return;
	PBLOCKHEADER block= (PBLOCKHEADER)((u32)data - BLOCKHEADER_SIZE);
	if(block->state != STATE_USED)
		return ;

	maos_disableInt();														//禁中断

	block->state = STATE_UNUSED;
	fusion_block(block,block->next);										//合并下块
	fusion_block(block->prev,block);										//合并上块

	maos_enableInt();														//开中断
}

/*
****************************************************************************//********************
*  函数: 从内存池中重新申请内存
*  参数: pool:内存池
*  参数: data:内存数据
*  参数: size:内存大小
*  返回: 无
****************************************************************************//********************
*/
void *maos_remalloc(MALLOCPOOL* pool,void *data,u32 size)
{
	void *res = maos_malloc(pool,size);										//申请新内存
	if(res)																	//申请 成功
	{
		memcpy(res,data,size);												//拷贝原内存
		maos_free(pool,data);												//释放原内存
	}
	
	return res;																//返回新内存
}

/*
****************************************************************************//********************
*  函数: 初始化内存池
*  参数: pool:内存池
*  返回: 无
****************************************************************************//********************
*/
void maos_malloc_init(MALLOCPOOL* pool)
{
	BLOCKHEADER *block = (BLOCKHEADER *)pool->poolBase;						//指向首块

	u32 size = (u32)pool->poolEnd - (u32)pool->poolBase + 1;				//内存池大小
	memset(block,0,size);													//初始化内存池
	block->next = NULL;														//无下块
	block->prev = NULL;														//无上块
	block->dataLen = size - BLOCKHEADER_SIZE;								//数据长度
	block->state = STATE_UNUSED;											//块状态：空闲中
	
}




