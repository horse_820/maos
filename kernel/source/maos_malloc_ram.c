/*
 * maos_malloc_ram.c	片内ram动态内存
 *
 *  Created on: 2021年1月20日
 *      Author: MCX
 */
#include "maos_malloc.h"


/*---------------------宏定义------------------------------------*/
#define		MALLOC_NAME		"MALLOC_VAR"
#define		POOL_BASE		0x20004000
#define		POOL_END		0x2000FFFF
#define		MINDATASIZE		64

/*-----------------------声名函数--------------------------------*/
static void* malloc(u32 size);
static void free(void *data);
static void* remalloc(void *data,u32 size) ;
static void init(void);
static void exit(void);
static u32 get_residue(void);

/*----------------------定义内存池--------------------------------*/
static MALLOCPOOL _pool ={
	.poolBase = POOL_BASE,
	.poolEnd = POOL_END,
	.minDataSize = MINDATASIZE,
	.enable = false,
};

/*---------------------定义动态内存--------------------------------*/
static MALLOC_OPS _ops =
{
	.malloc = malloc,
	.free = free,
	.remalloc = remalloc,
	.get_residue = get_residue,
};

/*---------------------注册动态内存--------------------------------*/
MALLOC_RIGISTER(var,MALLOC_NAME,&_ops,init,exit);

/*
****************************************************************************//********************
*  函数: 获取剩余内存
*  参数: 无
*  返回: 剩余内存
****************************************************************************//********************
*/
static u32 get_residue(void)
{
	return maos_malloc_residue(&_pool);
}

/*
****************************************************************************//********************
*  函数: 分配内存
*  参数: size:内存大小
*  返回: 内存数据
****************************************************************************//********************
*/
static void* malloc(u32 size)
{
	if(!_pool.enable)
		return NULL;
	
	return maos_malloc(&_pool,size);
}

/*
****************************************************************************//********************
*  函数: 释放内存
*  参数: data:内存数据
*  返回: 无
****************************************************************************//********************
*/
static void free(void *data)
{
	if(!_pool.enable)
		return;

	maos_free(&_pool,data);
}

/*
****************************************************************************//********************
*  函数: 重新申请内存
*  参数: data:内存数据
*  参数: size:内存大小
*  返回: 无
****************************************************************************//********************
*/
static void* remalloc(void *data,u32 size) 
{
	if(!_pool.enable)
		return NULL;

	return maos_remalloc(&_pool,data,size);
}


/*
****************************************************************************//********************
*  函数: 加载动态内存
*  参数: 无
*  返回: 对象
****************************************************************************//********************
*/
static void init(void)
{
	if(_pool.enable)
		return;
	_pool.enable = true;
	maos_malloc_init(&_pool);
}

/*
****************************************************************************//********************
*  函数: 卸载动态内存
*  参数: 无
*  返回: 无
****************************************************************************//********************
*/
static void exit(void)
{
	_pool.enable = false;
}



