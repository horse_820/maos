/*
 * maos_buffer.c	环形缓存（固定长度） 每个缓存独占一块动态内存 
 * 每条缓存 前2字节为 数据长度，
 * 引用 buffer/buffer.h 使用 Buffun 操作缓存
 * Created on: 2021年1月20日
 * modify on: 2023年3月30日:
 * 解决缓存忙时无法写入数据bug
 * Author: MCX
 */
#include "maos_buffer.h"
#include "maos_malloc.h"
#include "maos_sys.h"

/*-----------------------定义私有--------------------------------*/
typedef struct
{
	pBUFFER			next;						//下一缓存
	pMALLOC_OPS		mem;						//动态内存
	const char  	*name;						//缓存名称
	void			*buf;						//缓存开始位置
	void			*end;						//缓存结束位置（不包含本位置）
	void			*write;						//写缓存指针
	void			*read;						//读缓存指针
	uint16_t		size;						//一条缓存占内存数目
	uint16_t		num;						//已使用用缓存条数
	uint16_t		totalNum;					//缓存总条数
//	bool			busy;						//读缓存忙标志

}PROTECT;


/*-----------------------声名函数--------------------------------*/
static pBUFFER create(const char *name,const char *malloc, uint16_t size,uint16_t totalnum);
static void destory(pBUFFER obj);
static pBUFFER find(const char *name);

/*---------------------宏定义------------------------------------*/
#define	FUNDEFAULT	{\
						create,\
						destory,\
						find\
					}

/*-----------------------定义变量--------------------------------*/
BUFFERFUN	Tbuffer = FUNDEFAULT;
static pBUFFER 	firstObj=NULL;	
					

/*
****************************************************************************//********************
*  函数: 匹配缓存数据
*  参数: buf:缓存				
*  参数: data:数据
*  返回: 1：成功	0：失败
****************************************************************************//********************
*/
static bool match(pBUFFER obj,void *data,u16 len)
{
	PROTECT *protect = obj->protect;										//指向私有变量
	//确认参数
	if(!obj || !data || !len || !protect)
		return false;
	if(len>protect->size)						
		return false;
	
	u8 *p=protect->read;													//指向首条数据
	uint16_t num=protect->num;												//缓存中的数据条数
	uint16_t size=protect->size;											//一条数据长度

	while(num--)															//历遍所用数据
	{
		if(*(u16*)p== len && memcmp(&p[2],data,len)==0)											//匹配到数据
			return true;													//返回成功
		p = p+size;															//指向下一条
		if((u32)p >= (u32)protect->end)										//指针越界
			p = protect->buf;												//指向缓存开始位置
	}
	return false;															//返回失败
}

/*
****************************************************************************//********************
*  函数: 清空缓存数据
*  参数: obj:缓存对象				
*  返回: 无
****************************************************************************//********************
*/
static void clear(pBUFFER obj)
{
	PROTECT *protect = obj->protect;										//指向私有变量
	//确认参数
	if(!obj || !protect )
		return ;
	
	protect->write  	= protect->buf;										//写缓存指针
	protect->read  		= protect->buf;										//读缓存指针
	protect->num 		= 0;												//缓存条数

}


/*
****************************************************************************//********************
*  函数: 写一条缓存函数
*  参数: buf ：缓存头
*  参数: data ：写入数据
*  返回: 1：成功	0：失败
****************************************************************************//********************
*/
static bool	push(pBUFFER obj,void *data,u16 len)
{
	PROTECT *protect = obj->protect;										//指向私有变量
	//确认参数
	if(!obj || !data || !protect || len>protect->size-2 ||!len)
		return false;
	//确认有空缓存 
	if(protect->num>=protect->totalNum)
		return false;
	
	u8 *p = protect->write;													//指向数据长度
	*(u16*)p = len;															//写数据长度
	p+=2;																	//指向数据
	memcpy(p,data,len);														//写一条缓存
	protect->num++;															//更新已使用用缓存条数
	protect->write = (u8 *)protect->write + protect->size;					//指向下一条
	if(protect->write >= protect->end)										//指针越界
		protect->write = protect->buf;										//指向缓存开始位置

	return true;															//返回成功
}

/*
****************************************************************************//********************
*  函数: 读一条缓存函数
*  参数: buf ：缓存头
*  参数: data ：返回数据
*  返回: 数据长度
****************************************************************************//********************
*/
static u16	pop(pBUFFER obj,void *data)
{
	PROTECT *protect = obj->protect;										//指向私有变量
	//确认参数
	if(!obj || !data || !protect)
		return 0;
	//确认缓存有数据
	if(!protect->num || protect->num>protect->totalNum)
		return 0;
	
	//数据从缓存里出栈
	maos_disableInt();														//禁中断
	u8 *p = protect->read;													//指向数据长度
	u16 len = *(u16*)protect->read;											//读数据长度
	p+=2;																	//指向数据
	memcpy(data,p,len);														//读一条数据
	memset(protect->read,0,protect->size);									//清空缓存
	
	//更新缓存变量
	protect->num--;															//更新已使用用缓存条数
	protect->read = (u8 *)protect->read + protect->size;					//指向下一条
	if(protect->read >= protect->end)										//指针越界
		protect->read = protect->buf;										//指向缓存开始位置
	
	maos_enableInt();														//开中断
	return len;
}

/*
****************************************************************************//********************
*  函数: 创建缓存函数
*  参数: name ：缓存名称
*  参数: malloc ：动态内存
*  参数: size ：一条缓存大小
*  参数: totalnum ：缓存条数
*  返回: 缓存
****************************************************************************//********************
*/
static pBUFFER create(const char *name,const char *malloc, uint16_t size,uint16_t totalnum)
{
	//确认参数
	if(!name || !malloc || !size || !totalnum)
		return false;
	
	
	size += sizeof(u16);													//加上数据长度2字节
	//动态内存
	pMALLOC_OPS mem = Tmalloc.find(malloc);									//查找动态内存
	pMALLOC_OPS var = Tmalloc.find("MALLOC_VAR");							//查找动态内存
	if(!mem || !var)														//动态内存无效
		return NULL;														//返回空
	
	//申请内存
	uint32_t len=(size)  * totalnum;										//缓存大小+
	u8 *buf = mem->malloc(len);												//申请缓存
	pBUFFER object = var->malloc(sizeof(BUFFER));							//申请缓存头
	PROTECT	*protect = var->malloc(sizeof(PROTECT));						//申请私有变量
	if(!buf || !object || !protect)											//内存申请失败
	{
		mem->free(buf);														//释放缓存 
		var->free(object);													//释放缓存头
		var->free(protect);													//释放私有变量		
		return NULL;														//返回空
	}
	
	//初始变量
	memset(buf,0,len);														//清空缓存
	memset(object,0,sizeof(BUFFER));										//清空缓存对象
	memset(protect,0,sizeof(PROTECT));										//清空私有变量	
	//protect
	protect->name=name;														//缓存名称
	protect->mem=mem;														//动态内存
	protect->buf 		= buf;												//缓存开始位置
	protect->end 		= (u8*)protect->buf + size * totalnum;				//缓存结束位置（不包含本位置）
	protect->write  	= protect->buf;										//写缓存指针
	protect->read  		= protect->buf;										//读缓存指针
	protect->size		= size;												//一条缓存占内存数目
	protect->totalNum 	= totalnum;											//缓存总条数
	//object
	object->match = match;													//赋值匹配数据函数
	object->clear = clear;													//赋值清缓存函数
	object->pop = pop;														//赋值读缓存函数
	object->push = push;													//赋值写缓存函数
	object->protect = protect;												//私有变量
	
	//对象链表处理
	pBUFFER node = firstObj;												//指向首对象
	if(!node)																//无首对象
		firstObj = object;													//赋值首对象
	else																	//之前有对象
	{
		while(node && node->protect)										//匹配所有对象
		{
			protect = node->protect;										//指向私有变量
			if(protect->next)												//本节点非尾节点
				node = protect->next;										//下一节点
			else															//本节点为尾节点
			{
				protect->next = object;										//增加本对象
				break;														//结束匹配
			}
		}
	}
	
	return object;															//返回缓存
}


/*
****************************************************************************//********************
*  函数: 销毁对象函数
*  参数: object ：对象
*  返回: 无
****************************************************************************//********************
*/
static void destory(pBUFFER object)
{
	PROTECT *protect = object->protect;										//指向私有变量
	//确认参数
	if(!object || !protect)
		return;
	
	//对象链表删除本对象
	PROTECT *pt;
	pBUFFER node = firstObj;												//指向首对象
	if(node==object)														//本对象为首对象
		firstObj =  protect->next;											//首对象指向本对象下节点
	else																	//本对象非首对象
	{
		while(node && node->protect)										//匹配所有对象
		{
			pt = node->protect;												//指向私有变量
			if(pt->next == object)											//匹配到对象上节点
			{
				pt->next =protect->next;									//对象上节点的下节点 为 对象下节点
				break;														//结束匹配
			}
			node = pt->next;												//指向下一节点
		}
	}
	
	//动态内存
	pMALLOC_OPS mem = protect->mem;											//缓存所丰查找内存
	pMALLOC_OPS var = Tmalloc.find("MALLOC_VAR");							//查找动态内存
	if(!mem || !var)														//动态内存无效
		return;																//返回
	
	mem->free(protect->buf);												//释放缓存 
	var->free(protect);														//释放缓存头
	var->free(object);														//释放私有变量	
	object->protect = NULL;													//对象无效
}

/*
****************************************************************************//********************
*  函数: 查找缓存对象
*  参数: name ：对象名称
*  返回: 无
****************************************************************************//********************
*/
static pBUFFER find(const char *name)
{
	PROTECT *protect;
	pBUFFER node = firstObj;												//指向首对象
	while(node)																//匹配所有对象
	{
		protect = node->protect;											//指向相应的保护变量
		if(strcmp(name,protect->name)==0)									//匹配到端口名称
			return node;
		node = protect->next;												//指向下一对象
	}
	return NULL;	
}

//end of file.
