/*
 * maos_sys.c
 *	[移植] 开关中断函数,实现延时函数
 *  Created on: 2021年1月20日
 *  Author: MCX
 */
#include "maos_include.h" 

extern uint32_t SystemCoreClock; 
volatile uint32_t maos_tick;												//系统时钟 每ms计数1次
static volatile uint32_t maos_dint_counter=0;								//禁中断次数
static uint32_t fac_us=72;

//初始化延迟函数
//当使用ucos的时候,此函数会初始化ucos的时钟节拍
//SYSTICK的时钟设置为内核时钟
//SYSCLK:系统时钟频率,即CPU频率(rcc_c_ck),400Mhz
void delay_init(u16 SYSCLK)
{
	uint32_t reload;
	fac_us=SYSCLK;							//不论是否使用OS,fac_us都需要使用
 	SysTick->CTRL|=1<<2;					//SYSTICK使用内核时钟源,同CPU同频率	 
	reload=SYSCLK;							//每秒钟的计数次数 单位为M	   
	reload*=1000000/1000;					//1ms

	SysTick->CTRL|=1<<1;   					//开启SYSTICK中断
	SysTick->LOAD=reload; 					//每1/delay_ostickspersec秒中断一次	
	SysTick->CTRL|=1<<0;   					//开启SYSTICK    

}
//延时nms 
//nms:0~65535
void delay_ms(u16 nms)
{	 	 
	u32 tick = maos_tick;
	u32 time=0;
	while(time<nms)
	{
		if(maos_tick<tick)
			time = 0xFFFFFFFF-tick+maos_tick;
		else
			time = maos_tick - tick;
		
	};
}

void delay_us(u32 nus)
{	
	SysTick_Type oldvalue = *SysTick;		//
	
	u32 start =SysTick->VAL;	
	u32 num = nus*fac_us;					//延时需要的节拍数
	u32 value,time;							//延时结束时刻
	do
	{
		value = SysTick->VAL;
		if(value>=start)
			time = start + SysTick->LOAD - value;
		else
			time = start - value;
		if(time >= num)
			break;
	}while(1);	//等待时间到达  
		
}


/*
****************************************************************************//********************
*  函数: 系统tick时钟中断函数每ms中断一次
*  参数: 无
*  返回: 无
****************************************************************************//********************
*/
void SysTick_Handler(void)
{	

	maos_tick++;															//os时钟
	HAL_IncTick();
}

/*
****************************************************************************//********************
*  函数: 全局禁止中断
*  参数: 无
*  返回: 无
****************************************************************************//********************
*/
void maos_disableInt(void)
{
	if(maos_dint_counter==0)												//之前状态：中断开
		__ASM volatile("cpsid i");											//执行关中断 [移植]
	
	maos_dint_counter++;													//增加禁中断次数	
}

/*
****************************************************************************//********************
*  函数: 全局禁开中断
*  参数: 无
*  返回: 无

****************************************************************************//********************
*/
void maos_enableInt(void)
{
	if(maos_dint_counter==0)												//之前状态：中断开
		return;
	
	if(--maos_dint_counter==0)												//减少禁中断次数	
		__ASM volatile("cpsie i");											//执行开中断 [移植]
}


/*
****************************************************************************//********************
*  函数: 重启
*  参数: 无
*  返回: 无

****************************************************************************//********************
*/
void maos_rebot(void)
{
	SCB->AIRCR  = ((0x5FA << SCB_AIRCR_VECTKEY_Pos)      | 
                 (SCB->AIRCR & SCB_AIRCR_PRIGROUP_Msk) | 
                 SCB_AIRCR_SYSRESETREQ_Msk);                   /* Keep priority group unchanged */
	__DSB();                                                     /* Ensure completion of memory access */              
	while(1);  

}



//end of file.
