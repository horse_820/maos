/*
 * maos_list.c	链表 
 * 引用 maos_list.h 使用 Tlist 操作对象
 * Created on: 2021年3月9日
 * Author: MCX
 */
#include "maos_list.h"
#include "maos_malloc.h"
#include "maos_sys.h"

#define NODESIZE(x)	(sizeof(LISTNODE)-sizeof(void*)+x)


/*-----------------------定义私有--------------------------------*/
typedef struct
{
	pLIST			next;							//下一队列对象
	pMALLOC_OPS		mem;							//动态内存
	uint32_t		nodeNum;						//节点数目
	const char  	*name;							//队列名称	
	uint16_t		dataSize;						//节点数据长度
	pLISTNODE		first;							//首节点
	pLISTNODE		last;							//尾节点
}PROTECT;


/*-----------------------声名函数--------------------------------*/
static pLIST create(const char *name,const char *malloc,uint16_t dataSize);
static void destory(pLIST obj);
static pLIST find(const char *name);

/*---------------------宏定义------------------------------------*/
#define	FUNDEFAULT	{\
						create,\
						destory,\
						find\
					}

/*-----------------------定义变量--------------------------------*/
TLISTFUN	Tlist = FUNDEFAULT;
static pLIST 	firstObj=NULL;	
					



/*
****************************************************************************//********************
*  函数: 匹配对象数据
*  参数: obj:对象				
*  参数: data:数据
*  返回: 1：成功	0：失败
****************************************************************************//********************
*/
static pLISTNODE match(pLIST obj,void *data,u16 len)
{
	PROTECT *protect = obj->protect;										//指向私有变量
	//确认参数
	if(!obj || !data || !len || !protect)
		return NULL;
	pLISTNODE node = protect->first;										//指向首节点
	while(node)																//历遍所有节点
	{
		if(memcmp(data,&node->data,len)==0)									//匹配到数据
			return node;													//返回成功

		node = node->next;													//下一节点
	}
	
	return NULL;															//返回失败
}

/*
****************************************************************************//********************
*  函数: 添加数据函数
*  参数: obj ：链表对象
*  参数: data ：数据
*  返回: 链表节点
****************************************************************************//********************
*/
static pLISTNODE add(pLIST obj,void *data)
{
	PROTECT *protect = obj->protect;										//指向私有变量
	//确认参数
	if(!obj || !data || !protect )											//参数错误
		return NULL;														//返回空节点
	
	pLISTNODE node = protect->mem->malloc(NODESIZE(protect->dataSize));		//申请节点内存
	if(!node )																//申请失败
		return NULL;														//返回空节点
	
	memcpy(&node->data,data,protect->dataSize);								//写入数据
	node->next = NULL;														//下一节点为空
	node->prev = protect->last;												//上一节点为链表尾节点
	if(protect->last)														//链表有数据
		protect->last->next=node;											//在尾部添加节点
	else																	//链表无数据
		protect->first =node;												//更新首节点
	protect->last = node;													//更新尾节点
	protect->nodeNum++;														//节点数目

	
	return node;															//节点
}

/*
****************************************************************************//********************
*  函数: 删除节点函数
*  参数: obj ：链表对象
*  参数: node ：链表节点
*  返回: 1：成功		0：失败
****************************************************************************//********************
*/
static bool	del(pLIST obj,pLISTNODE node)
{
	PROTECT *protect = obj->protect;										//指向私有变量
	//确认参数
	if(!obj || !node || !protect)
		return false;
	
	if(!protect->first)														//链表空
		return false;														//返回空

	//节点变量更新
	if(node->prev)															//非首节点
		node->prev->next =  node->next;										//上一节点链接到下一节点
	else																	//首节点
		protect->first = node->next;										//下一节点更新为首节点
	if(node->next)															//下一节点存在
		node->next->prev = node->prev;										//下一节点链接到上一节点
	else																	//本节点为尾节点
		protect->last = node->prev;											//上一节点更新为尾节点

	if(protect->nodeNum)													//有节点
		protect->nodeNum--;													//节点数目
	protect->mem->free(node);												//释放节点内存

	return true;															//返回成功
}

/*
****************************************************************************//********************
*  函数: 历遍操作
*  参数: obj ：链表对象
*  参数: handle ：操作回调函数
*  参数: arg ：回调函数参数
*  返回: 无
****************************************************************************//********************
*/
static void traversal(pLIST obj,HANDLE handle,void *arg)
{
	PROTECT *protect = obj->protect;										//指向私有变量
	//确认参数
	if(!obj ||   !protect ||!handle)
		return ;

	pLISTNODE	node = protect->first;										//指向首节点 
	
	while(node)																//历遍所有节点
	{
		if(handle(node,arg))												//回调处理
			return;															//结束操作

		node = node->next;													//指向下一节点
	}
}

/*
****************************************************************************//********************
*  函数: 获取链表节点数目
*  参数: obj ：链表对象
*  返回: 节点数目
****************************************************************************//********************
*/
static uint16_t get_num(pLIST obj)
{
	PROTECT *protect = obj->protect;										//指向私有变量
	if(!obj ||   !protect )
		return 0;
	
	return protect->nodeNum;
}	

/*
****************************************************************************//********************
*  函数: 获取链表首节点
*  参数: obj ：链表对象
*  返回: 首节点
****************************************************************************//********************
*/
static pLISTNODE get_first(pLIST obj)
{
	PROTECT *protect = obj->protect;										//指向私有变量
	if(!obj ||   !protect )
		return NULL;
	
	return protect->first;
}
/*
****************************************************************************//********************
*  函数: 创建链表
*  参数: name ：链表名称
*  参数: malloc ：动态内存名称
*  参数: dataSize ：数据长度	
*  返回: 链表
****************************************************************************//********************
*/
static pLIST create(const char *name,const char *malloc,uint16_t dataSize)
{
	//确认参数
	if(!name || !malloc )
		return false;
	
	//动态内存
	pMALLOC_OPS mem = Tmalloc.find(malloc);									//查找动态内存
	pMALLOC_OPS var = Tmalloc.find("MALLOC_VAR");							//查找动态内存
	if(!mem || !var)														//动态内存无效
		return NULL;														//返回空
	
	//申请内存
	pLIST object = var->malloc(sizeof(LIST));								//申请类内存
	PROTECT	*protect = var->malloc(sizeof(PROTECT));						//申请私有变量内存
	if(!object || !protect)													//内存申请失败
	{
		var->free(object);													//释放对象头
		var->free(protect);													//释放私有变量		
		return NULL;														//返回空
	}
	
	//初始变量
	//protect
	memset(protect,0,sizeof(PROTECT));										//清空私有变量
	protect->mem = mem;														//动态内存
	protect->name = name;													//对象名称
	protect->dataSize = dataSize;											//节点数据长度
	
	//object
	memset(object,0,sizeof(LIST));											//清空对象对象
	object->match = match;													//赋值匹配数据函数
	object->add = add;														//赋值添加节点函数
	object->del = del;														//赋值删除节点函数
	object->traversal = traversal;											//赋值历遍操作函数
	object->get_num = get_num;												//赋值获取链表节点数目函数
	object->get_first = get_first;											//赋值获取链表首节点函数
	object->protect = protect;												//赋值私有变量
	
	//对象链表处理
	pLIST obj = firstObj;													//指向首对象
	if(!obj)																//无首对象
		firstObj = object;													//赋值首对象
	else																	//之前有对象
	{
		while(obj && obj->protect)											//匹配所有对象
		{
			protect = obj->protect;											//指向私有变量
			if(protect->next)												//本节点非尾节点
				obj = protect->next;										//下一节点
			else															//本节点为尾节点
			{
				protect->next = object;										//增加本对象
				break;														//结束匹配
			}
		}
	}
	
	return object;															//返回对象
}


/*
****************************************************************************//********************
*  函数: 销毁对象函数
*  参数: object ：对象
*  返回: 无
****************************************************************************//********************
*/
static void destory(pLIST object)
{
	PROTECT *pt;
	PROTECT *protect = object->protect;										//指向私有变量
	//确认参数
	if(!object || !protect)
		return;
	
	//释放所有数据内存
	pLISTNODE node=protect->first;											//指向首节点
	while(node)
	{
		protect->mem->free(node);											//释放节点内存
		node = node->next;													//下一节点
	};
	
	
	//对象链表删除本对象
	pLIST obj = firstObj;													//指向首对象
	if(obj==object)															//本对象为首对象
		firstObj =  protect->next;											//首对象指向本对象下节点
	else																	//本对象非首对象
	{
		while(obj && obj->protect)											//匹配所有对象
		{
			pt = obj->protect;												//指向私有变量
			if(pt->next == object)											//匹配到对象上节点
			{
				pt->next =protect->next;									//对象上节点的下节点 为 对象下节点
				break;														//结束匹配
			}
			obj = pt->next;													//指向下一节点
		}
	}
	
	//动态内存
	pMALLOC_OPS var = Tmalloc.find("MALLOC_VAR");							//查找动态内存
	if(!var)																//动态内存无效
		return;																//返回
	
	var->free(protect);														//释放对象头
	var->free(object);														//释放私有变量
	object->protect = NULL;													//对象无效
}

/*
****************************************************************************//********************
*  函数: 查找对象函数
*  参数: name ：对象名称
*  返回: 无
****************************************************************************//********************
*/
static pLIST find(const char *name)
{
	PROTECT *protect;
	pLIST obj = firstObj;													//指向首对象
	while(obj)																//匹配所有对象
	{
		protect = obj->protect;												//指向相应的保护变量
		if(strcmp(name,protect->name)==0)									//匹配到端口名称
			return obj;
		obj = protect->next;												//指向下一对象
	}
	return NULL;	
}

//end of file.
