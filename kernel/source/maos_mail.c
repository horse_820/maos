/*
* maos_mail.c 邮箱任务
 *
 *  Created on: 2021年1月26日
 *      Author: MCX
 */

#include "maos_mail.h"
#include "maos_malloc.h"
#include "maos_task.h"

#define		TASKMAXNUM		16				//任务最大数目
#define		LOOPTIMEMS		20				//最小等待周期(ms)

//--------------------------------定义等待结构体----------------------------------//
typedef struct
{
	bool				enable;				//等待使能
	uint32_t			counter;			//等待计数器
	uint32_t			time;				//等待时间
	char				str[64];			//等待接收字符串
	FMAIL				back;				//回调函数
}WAITTASK;

//-------------------------------定义类保护变量结构体------------------------------//
typedef struct
{
	WAITTASK			task[TASKMAXNUM];	//任务等待寄存器
	uint8_t 			num;				//任务数
}PROTECT;

/*----------------------------------声名函数--------------------------------------*/
static void receive(char *str);
static uint8_t add(char *str,uint32_t time,FMAIL back);
static void del(uint8_t index);
static void clear(void);
static void loop(void);

//-------------------------------定义变量-----------------------------------------//
static PROTECT protect;
MAILFUN Tmail={
	.recv = receive,
	.add = add,
	.del = del,
	.clear = clear,
};


	
/*
****************************************************************************//********************

*  函数: 接收处理
*  参数: str:接收的字符串
*  返回: 无
****************************************************************************//********************
*/
static void receive(char *str)
{
	char *res;
	uint8_t i;
	WAITTASK *task;
	bool clear;
	

	for(i=0;i<TASKMAXNUM;i++)												//历遍所有任务
	{
		task = &protect.task[i];											//指向相应任务
		if(!task->enable)													//任务有效
			continue;														//继续匹配
		res =strstr(str,task->str);											//匹配字符串
		if(!res)															//匹配失败
			continue;														//继续匹配

		if(task->back)														//有回调函数						
		{
			clear = task->back(i+1,res);									//执行回调
			if(clear)														//回调函数要求清除任务
				del(i+1);													//消除任务
		}
		break;
	}
}

/*
****************************************************************************//********************
*  函数: 添加任务
*  参数: str:等待结果内容
*  参数: time:等待时间
*  参数: back:回调结果函数
*  返回: 任务序号 从1开始编码
****************************************************************************//********************
*/
static uint8_t add(char *str,uint32_t time,FMAIL back)
{
	uint8_t i,handle=0;
	WAITTASK *task;
	
	
	//确认参数
	uint16_t len =strlen(str);												//结果长度
	if(!len || len>=64 ||protect.num >=TASKMAXNUM)							
		return 0;
		
	for(i=0;i<TASKMAXNUM;i++)												//历遍所有任务
	{
		task = &protect.task[i];											//指向相应任务
		if(!task->enable)													//任务有效
			continue;														//继续匹配

		task->enable = true;												//使能任务				
		task->time = time;													//保存任务时间
		task->counter = 0;													//初始计数器
		task->back = back;													//保存回调函数
		strcpy(task->str,str);												//保存任务名
		protect.num++;														//增加任务数
		handle = i+1;														//返回任务序号
		break;
	}
	
	if(protect.num==1)
		Task.pfn_add(loop,TASK_IMPORTANT,LOOPTIMEMS);						//增加周期函数
	
	return handle;
}

/*
****************************************************************************//********************
*  函数: 添加任务
*  参数: index:任务序号
*  返回: 无
****************************************************************************//********************
*/
static void del(uint8_t index)
{
	//确认参数
	if(!index || index>TASKMAXNUM || !protect.num)
		return;
		
	WAITTASK *task = &protect.task[index-1];								//指向相应任务
	memset(task,0,sizeof(WAITTASK));										//清空任务
	if(--protect.num==0)													//减少任务数目
		Task.pfn_del(loop);													//删除周期函数
}

/*
****************************************************************************//********************
*  函数: 清空所有任务
*  参数: 无
*  返回: 无
****************************************************************************//********************
*/
static void clear(void)
{
	memset(&protect,0,sizeof(PROTECT));										//清除所有任务
	Task.pfn_del(loop);														//删除周期函数
}

/*
****************************************************************************//********************
*  函数: 周期函数
*  参数: 无
*  返回: 无
****************************************************************************//********************
*/
static void loop(void)
{
	uint8_t i;
	WAITTASK *task;
	bool clear;
	
	for(i=0;i<TASKMAXNUM;i++)												//历遍所有任务
	{
		task = &protect.task[i];											//指向相应任务
		if(!task->enable)													//任务无效
			continue;														//下一任务
		
		task->counter+=LOOPTIMEMS;											//更新计数器
		if(task->counter < task->time)										//未超时
			continue;														//下一任务

		if(task->back)														//有回调函数						
		{
			clear = task->back(i+1,NULL);									//执行回调
			if(clear)														//回调函数要求清除任务
				del(i+1);													//消除任务
		}
		else																//无回调函数
			del(i+1);														//消除任务
	}
}

//end of file.

