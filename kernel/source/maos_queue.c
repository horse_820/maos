/*
 * maos_queue.c	队列 每条数据占一个内存，数据之间单向链表 
 * 每条数据带一数据头，
 * 引用 maos_queue.h 使用 Tqueue 操作对象
 * Created on: 2021年1月20日
 * modify on: 2023年3月30日:
 * 解决缓存忙时无法写入数据bug

 * Author: MCX
 */
#include "maos_queue.h"
#include "maos_malloc.h"
#include "maos_sys.h"

//-------------------------定义队列节点结构体--------------------//
typedef	struct _queuenode	QUEUENODE;
typedef struct _queuenode	*pQUEUENODE;
struct _queuenode	
{
	pQUEUENODE				next;
	uint32_t				len;
	void					*data;
};
#define HEADERSIZE	(sizeof(QUEUENODE)-sizeof(void*))

//--------------------------定义列表结构体--------------------//
typedef	struct _list	LIST;
typedef struct _list	*pLIST;
struct _list
{
	pQUEUENODE		first;							//首节点
	pQUEUENODE		last;							//尾节点
};

/*-----------------------定义私有--------------------------------*/
typedef struct
{
	pQUEUE			next;							//下一队列对象
	pMALLOC_OPS		mem;							//动态内存
	const char  	*name;							//队列名称
	LIST			list;							//队列
}PROTECT;


/*-----------------------声名函数--------------------------------*/
static pQUEUE create(const char *name,const char *malloc);
static void destory(pQUEUE obj);
static pQUEUE find(const char *name);

/*---------------------宏定义------------------------------------*/
#define	FUNDEFAULT	{\
						create,\
						destory,\
						find\
					}

/*-----------------------定义变量--------------------------------*/
TQUEUEFUN	Tqueue = FUNDEFAULT;
static pQUEUE 	firstObj=NULL;	
					
/*
****************************************************************************//********************
*  函数: 向列表添加尾节点
*  参数: list:列表			
*  参数: node:节点
*  返回: 无
****************************************************************************//********************
*/
static void list_add_last(pLIST list,pQUEUENODE node)
{
	//确认参数
	if(!list || !node)
		return;
	
	if(list->last == NULL)													//链表空
		list->first = node;													//更新首节点
	else																	//链表有节点
		list->last->next = node;											//添加节点到链表尾
	
	list->last = node;														//更新尾节点
}

/*
****************************************************************************//********************
*  函数: 列表删除首节点
*  参数: list:列表			
*  返回: 删除节点
****************************************************************************//********************
*/
static void list_del_first(pLIST list)
{
	pQUEUENODE first = list->first;											//提取原首节点
	list->first = first->next;												//更新首节点
	if(list->last == first)													//原来只有一个节点
		list->last = NULL;													//删除原尾节点

}

/*
****************************************************************************//********************
*  函数: 列表匹配节点
*  参数: list:列表	
*  参数: data:数据
*  参数: len:数据长度
*  返回: 节点
****************************************************************************//********************
*/
static pQUEUENODE list_match_node(pLIST list,void *data,u16 len)
{
	//确认参数
	if(!list || !data || !len)
		return NULL;

	if(list->first == NULL)													//链表空
		return NULL;
	
	pQUEUENODE node = list->first;											//指向首节点
	while(node)																//历遍所有节点
	{
		if(node->len ==len)													//匹配到数据长度
		{
			if(memcmp(data,&node->data,len)==0)								//匹配到数据
				return node;												//返回匹配到节点
		}
		node = node->next;													//下一节点
	}
	
	return NULL;															//返回匹配失败
}


/*
****************************************************************************//********************
*  函数: 匹配对象数据
*  参数: obj:对象				
*  参数: data:数据
*  返回: 1：成功	0：失败
****************************************************************************//********************
*/
static bool match(pQUEUE obj,void *data,u16 len)
{
	PROTECT *protect = obj->protect;										//指向私有变量
	//确认参数
	if(!obj || !data || !len || !protect)
		return false;
	
	pQUEUENODE node = NULL;													//定义空节点
	node = list_match_node(&protect->list,data,len);						//匹配主链表
	if(node)																//匹配成功
		return true;														//返回成功
	
	
	return false;															//返回失败
}

/*
****************************************************************************//********************
*  函数: 写一条数据 （中断接收时写入）
*  参数: obj ：对列
*  参数: data ：写入数据
*  参数: len ：数据长度
*  返回: 1：成功	0：失败
****************************************************************************//********************
*/
static bool	push(pQUEUE obj,void *data,u16 len)
{
	PROTECT *protect = obj->protect;										//指向私有变量

	//确认参数
	if(!obj || !data || !protect || !len)
		return false;
	
	pQUEUENODE node = protect->mem->malloc(len+HEADERSIZE);					//申请节点内存
	if(!node )																//无内存
		return false;														//返回失败

	//保存数据到内存里
	node->next = NULL;														//下一节点为空
	node->len = len;														//写入数据长度
	memcpy(&node->data,data,len);											//写入数据
	
	list_add_last(&protect->list,node);										//节点写入链表里
	return true;															//返回成功

}

/*
****************************************************************************//********************
*  函数: 读一条数据 （周期调用）
*  参数: buf ：对象头
*  参数: obj ：对列
*  参数: data ：写入数据
*  返回: 数据长度
****************************************************************************//********************
*/
static u16	pop(pQUEUE obj,void *data)
{
	PROTECT *protect = obj->protect;										//指向私有变量
	//确认参数
	if(!obj || !data || !protect)
		return 0;
	
	
	u16 len = 0;
	pLIST list = &protect->list;
	pQUEUENODE node=list->first;											//指向首节点
	if(!node)
		return 0;

	maos_disableInt();														//禁中断
	memcpy(data,&node->data,node->len);										//返回数据
	len = node->len;														//返回数据长度
	list_del_first(list);													//从链表上删除首节点
	maos_enableInt();														//开中断
	protect->mem->free(node);												//释放节点内存 过程不允许中断
	return len;
}

/*
**************************************************************************************************
*  函数: 队列是否为空
*  参数: obj ：队列
*  返回: 1：空  0 非空
**************************************************************************************************
*/
static bool is_empyt(pQUEUE obj)
{
	PROTECT *protect = obj->protect;										//指向私有变量
	//确认参数
	if(!obj ||  !protect)
		return false;
	
	if(!protect->list.first)												//链表上无节点
		return true;
	else
		return false;
	
}

/*
****************************************************************************//********************
*  函数: 创建对象函数
*  参数: name ：对象名称
*  参数: malloc ：动态内存
*  返回: 对象
****************************************************************************//********************
*/
static pQUEUE create(const char *name,const char *malloc)
{
	//确认参数
	if(!name || !malloc )
		return false;
	
	//动态内存
	pMALLOC_OPS mem = Tmalloc.find(malloc);									//查找动态内存
	pMALLOC_OPS var = Tmalloc.find("MALLOC_VAR");							//查找动态内存
	if(!mem || !var)														//动态内存无效
		return NULL;														//返回空
	
	//申请内存
	pQUEUE object = var->malloc(sizeof(QUEUE));								//申请类内存
	PROTECT	*protect = var->malloc(sizeof(PROTECT));						//申请私有变量内存
	if(!object || !protect)													//内存申请失败
	{
		var->free(object);													//释放对象头
		var->free(protect);													//释放私有变量		
		return NULL;														//返回空
	}
	
	//初始变量
	//protect
	memset(protect,0,sizeof(PROTECT));										//清空私有变量
	protect->mem = mem;														//动态内存
	protect->name = name;													//对象名称
	//object
	memset(object,0,sizeof(QUEUE));											//清空对象对象
	object->match = match;													//赋值匹配数据函数
	object->pop = pop;														//赋值读对象函数
	object->push = push;													//赋值写对象函数
	object->protect = protect;												//私有变量
	object->is_empyt = is_empyt;											//赋值函数
	
	//对象链表处理
	pQUEUE obj = firstObj;													//指向首对象
	if(!obj)																//无首对象
		firstObj = object;													//赋值首对象
	else																	//之前有对象
	{
		while(obj && obj->protect)											//匹配所有对象
		{
			protect = obj->protect;											//指向私有变量
			if(protect->next)												//本节点非尾节点
				obj = protect->next;										//下一节点
			else															//本节点为尾节点
			{
				protect->next = object;										//增加本对象
				break;														//结束匹配
			}
		}
	}
	
	return object;															//返回对象
}

/*
****************************************************************************//********************
*  函数: 销毁对象函数
*  参数: object ：对象
*  返回: 无
****************************************************************************//********************
*/
static void destory(pQUEUE object)
{
	PROTECT *pt;
	PROTECT *protect = object->protect;										//指向私有变量
	//确认参数
	if(!object || !protect)
		return;
	
	//释放所有数据内存
	pQUEUENODE node=protect->list.first;									//指向首节点
	while(node)
	{
		protect->mem->free(node);											//释放节点内存
		node = node->next;													//下一节点
	};
	
	//对象链表删除本对象
	pQUEUE obj = firstObj;													//指向首对象
	if(obj==object)															//本对象为首对象
		firstObj =  protect->next;											//首对象指向本对象下节点
	else																	//本对象非首对象
	{
		while(obj && obj->protect)											//匹配所有对象
		{
			pt = obj->protect;												//指向私有变量
			if(pt->next == object)											//匹配到对象上节点
			{
				pt->next =protect->next;									//对象上节点的下节点 为 对象下节点
				break;														//结束匹配
			}
			obj = pt->next;													//指向下一节点
		}
	}
	
	//动态内存
	pMALLOC_OPS var = Tmalloc.find("MALLOC_VAR");							//查找动态内存
	if(!var)																//动态内存无效
		return;																//返回
	
	var->free(protect);														//释放对象头
	var->free(object);														//释放私有变量
	object->protect = NULL;													//对象无效
}

/*
****************************************************************************//********************
*  函数: 查找对象函数
*  参数: name ：对象名称
*  返回: 无
****************************************************************************//********************
*/
static pQUEUE find(const char *name)
{
	PROTECT *protect;
	pQUEUE obj = firstObj;													//指向首对象
	while(obj)																//匹配所有对象
	{
		protect = obj->protect;												//指向相应的保护变量
		if(strcmp(name,protect->name)==0)									//匹配到端口名称
			return obj;
		obj = protect->next;												//指向下一对象
	}
	return NULL;	
}

//end of file.

