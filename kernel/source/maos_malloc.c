/*
 * maos_mallo.c
 *
 *  Created on: 2021年1月20日
 *      Author: mcx
 */
#include "maos_malloc.h"


//----------------------声名函数----------------------------------------//
static pMALLOC_OPS rigister(const char *name);
static void destroy(const char *name);
//----------------------建立变量----------------------------------------//
TMALLOCFUN  Tmalloc=
{
	.rigister = rigister,
	.unregister = destroy,
	.find = rigister,
};
	
//----------------------引用变量----------------------------------------//
extern const unsigned int mallocTable$$Base;								//引用动态内存列表
extern const unsigned int mallocTable$$Limit;								//引用动态内存列表


/*
****************************************************************************//********************
*  函数: 注册对象函数
*  参数: name 对象名称
*  返回: 对象
****************************************************************************//********************
*/
static pMALLOC_OPS rigister(const char *name)
{
	//参数确认
	if(!name)
		return NULL;	

	//从列表里查找对象
	MALLOCREG *end=(MALLOCREG*)&mallocTable$$Limit;							//指向动态内存列表尾
	MALLOCREG *reg=(MALLOCREG*)&mallocTable$$Base;							//指向动态内存列表首

	while((u32)reg<(u32)end)												//历遍所有动态内存
	{
		if(strcmp(reg->name,name)==0)										//匹配到名称
			break;															//停止匹配
		reg++;																//下一动态内存
	}
	if((u32)reg>=(u32)end)													//越界未匹配到
		return NULL;														//返回
	reg->init();															//初始化动态内存

	return reg->ops;														//返回对象操作集
}

/*
****************************************************************************//********************
*  函数: 销毁对象函数
*  参数: name 对象名称
*  返回: 对象
****************************************************************************//********************
*/
static void destroy(const char *name)
{
	//参数确认
	if(!name)
		return ;	

	//从列表里查找对象
	MALLOCREG *end=(MALLOCREG*)&mallocTable$$Limit;							//指向动态内存列表尾
	MALLOCREG *reg=(MALLOCREG*)&mallocTable$$Base;							//指向动态内存列表首

	while((u32)reg<(u32)end)												//历遍所有动态内存
	{
		if(strcmp(reg->name,name)==0)										//匹配到名称
			break;															//停止匹配
		reg++;																//下一动态内存
	}
	if((u32)reg>=(u32)end)													//越界未匹配到
		return ;															//返回
	reg->exit();															//销毁动态内存
}

/*
****************************************************************************//********************
*  函数: 动态内存初始化
*  参数: 无
*  返回: 无
****************************************************************************//********************
*/
void malloc_init(void)
{
	rigister("MALLOC_RAM");
}





