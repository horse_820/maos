/*
 * maos_tree.c	二叉树
 * 引用 maos_tree.h 使用 Buffun 操作对象
 * Created on: 2021年2月8日
 * Author: MCX
 */
#include "maos_tree.h"
#include "maos_buffer.h"
#include "maos_malloc.h"

#define NODEMAXNUM	256
#define HEADERSIZE	(sizeof(TREENODE)-sizeof(void*))
	
/*-----------------------定义私有--------------------------------*/
typedef struct
{
	const char 		*name;						//对象名称
	pTREE			next;						//下一对象
	uint32_t		dataLen;					//数据长度
//	pMALLOC_OPS		mem;						//动态内存
	pBUFFER			stack;						//堆栈
	
}PROTECT;

/*-----------------------声名函数--------------------------------*/
static pTREE create(const char *name,const char *malloc,uint32_t dataLen);
static void destory(pTREE obj);
static pTREE find(const char *name);

/*---------------------宏定义------------------------------------*/
#define	FUNDEFAULT	{\
						create,\
						destory,\
						find\
					}

/*-----------------------定义变量--------------------------------*/
TTREEFUN	Ttree = FUNDEFAULT;
static pTREE 	firstObj=NULL;	

/*
****************************************************************************//********************
*  函数: 确认添加在树上
*  参数: obj:树	
*  参数: data:节点			
*  返回: 1：在  0：不在
****************************************************************************//********************
*/	
static bool IsNodeOnTree(pTREE obj,pTREENODE data)
{
	PROTECT *protect = obj->protect;										//指向私有变量
	//确认参数
	if(!obj ||  !data || !protect)
		return false;	
	
	pTREENODE node = obj->rootNode;											//指向根节点
	pBUFFER stack = protect->stack;											//指向堆栈
	stack->clear(stack);													//清缓存
	pTREENODE stackNode;														//定义栈节点变量
	bool match = false;														//定义匹配结果
	while(node)																//历遍所有节点
	{
		if(node == data)													//匹配到数据
		{
			match = true;													//标记匹配成功
			break;															//退出匹配
		}
		
		if(node->left)	stack->push(stack,&node->left,sizeof(pTREENODE));	//左节点入栈
		if(node->right)	stack->push(stack,&node->right,sizeof(pTREENODE));	//右节点入栈		
		if(0==stack->pop(stack,&stackNode))									//取出下一节点
			node = NULL;													//无节点线束
		else
			node = stackNode;												//指向出栈节点
	}	

	return match;															//返回结果	
}

/*
****************************************************************************//********************
*  函数: 查找节点父母
*  参数: obj:树	
*  参数: data:节点			
*  返回: 1：在  0：不在
****************************************************************************//********************
*/	
static pTREENODE find_parent(pTREE obj,pTREENODE data)
{
	PROTECT *protect = obj->protect;										//指向私有变量
	//确认参数
	if(!obj ||  !data || !protect)
		return false;	
	
	pTREENODE node = obj->rootNode;											//指向根节点
	pTREENODE child;														//定义节点
	pBUFFER stack = protect->stack;											//指向堆栈
	stack->clear(stack);													//清缓存
	pTREENODE stackNode;													//定义栈节点变量
	while(node)																//历遍所有节点
	{
		child = node->left;													//本节点的首子女
		
		while(child)														//历遍所有子女
		{
			if(child == data)												//匹配到本节点的首子女
				return node;												//返回本节点
			child = child->right;											//下一子女
		}

		if(node->left)	stack->push(stack,&node->left,sizeof(pTREENODE));	//左节点入栈
		if(node->right)	stack->push(stack,&node->right,sizeof(pTREENODE));	//右节点入栈
		if(0==stack->pop(stack,&stackNode))									//取出下一节点
			node = NULL;													//无节点线束
		else
			node = stackNode;												//指向出栈节点		
	}	
	  
	return NULL;															//返回结果	
}

/*
****************************************************************************//********************
*  函数: 添加节点
*  参数: obj:树	
*  参数: parent:父节点			
*  参数: data:数据
*  返回: 节点
****************************************************************************//********************
*/					
static pTREENODE addNode(pTREE obj,pTREENODE parent,void *data)
{
	PROTECT *protect = obj->protect;										//指向私有变量
	//确认参数
	if(!obj ||  !data || !protect)
		return NULL;
	if(parent && !IsNodeOnTree(obj,parent))									//确认 parent 在 tree 下
		return NULL;														//返回失败
	
	//申请节点内存
	u32 nodeSize = HEADERSIZE + protect->dataLen;							//节点内存大小
	pTREENODE node = obj->mem->malloc(nodeSize);							//申请节点内存
	if(!node)
		return NULL;
	
	//赋值节点内容
	node->right = NULL;														//初始右节点
	node->left = NULL;														//初始左节点
	memcpy(&node->data,data,protect->dataLen);								//赋值数据
	
	//确认父节点
	if(!parent )															//无父节点
	{
		parent = obj->rootNode;												//挂载在根节点上
		if(!parent)															//无根节点
		{
			obj->rootNode = node;											//添加新节点
			return node;													//返回成功
		}
	}

	
	//将节点挂载在父节点上
	pTREENODE clild = parent->left;											//指向首子女
	pTREENODE brother;														//定义兄弟节点
	if(!clild)																//父母无子女
	{
		parent->left = node;												//添加新节点
		return node;														//返回成功
	}
	while(clild)															//历遍所有节点
	{
		brother = clild->right;												//下一兄弟
		if(!brother)														//无兄弟
			clild->right = node;											//添加新节点
		clild = brother;													//指向下一兄弟
	}
	
	return node;
}	

/*
****************************************************************************//********************
*  函数: 删除节点
*  参数: obj:树	
*  参数: node:节点			
*  返回: 无
****************************************************************************//********************
*/
static void delNode(pTREE obj,pTREENODE node)
{
	PROTECT *protect = obj->protect;										//指向私有变量
	//确认参数
	if(!obj ||  !node || !protect)
		return ;
	if(!IsNodeOnTree(obj,node))												//确认节点在 tree 下
		return ;															//返回
	
	//
	
	
	//释放子女节点内存
	pBUFFER stack = protect->stack;											//指向堆栈
	stack->clear(stack);													//清缓存
	pTREENODE stackNode;													//定义栈节点变量
	pTREENODE	clild = node->left;											//临时节点
	while(clild)															//历遍所有子节点
	{
		obj->mem->free(clild);											//释放内存
		if(clild->left)	stack->push(stack,&clild->left,sizeof(pTREENODE));	//左节点入栈
		if(clild->right)stack->push(stack,&clild->right,sizeof(pTREENODE));	//右节点入栈
		if(0==stack->pop(stack,&stackNode))									//取出下一节点
			clild = NULL;													//无节点线束
		else
			clild = stackNode;												//指向出栈节点	
	}	
	
	//从树上删除本节点
	obj->mem->free(node);												//释放本节点内存
	pTREENODE parent = find_parent(obj,node);								//找出其父节点
	if(!parent)																//根节点
	{
		obj->rootNode = NULL;												//删除根节点
		return;
	}
	if(parent->left==node)													//首子女为匹配目标
	{
		parent->left = node->right;											//删除本节点
		return;																//返回
	}
	pTREENODE brother,child = parent->left;									//定义兄弟、儿子
	while(child)															//历遍所有子女
	{
		brother = child->right;												//指向兄弟
		if(brother == node)													//兄弟为匹配目标
		{
			child->right = node->right;										//删除本节点
			return;															//返回
		}
		child = brother;													//下一子女 
	}
}

/*
****************************************************************************//********************
*  函数: 历遍操作
*  参数: obj ：树对象
*  参数: node ：开始节点,默认指向根结点 
*  参数: handle ：操作回调函数
*  参数: arg ：回调函数参数
*  返回: 无
****************************************************************************//********************
*/
static void traversal(pTREE obj,pTREENODE node,HANDLE handle,void *arg)
{
	PROTECT *protect = obj->protect;										//指向私有变量
	//确认参数
	if(!obj ||   !protect ||!handle)
		return ;
	if(!node)
		node = obj->rootNode;												//默认指向根结点 
	
	pBUFFER stack = protect->stack;											//指向堆栈
	stack->clear(stack);													//清缓存
	pTREENODE stackNode;														//定义栈节点变量
	while(node)																//历遍所有节点
	{
		if(handle(node,arg))												//回调处理
			return;															//结束操作

		if(node->left)	stack->push(stack,&node->left,sizeof(pTREENODE));	//左节点入栈
		if(node->right)	stack->push(stack,&node->right,sizeof(pTREENODE));	//右节点入栈
		if(0==stack->pop(stack,&stackNode))									//取出下一节点
			node = NULL;													//无节点线束
		else
			node = stackNode;												//指向出栈节点
	}
}

/*
****************************************************************************//********************
*  函数: 创建树
*  参数: name ：树名称
*  参数: malloc ：动态内存名称
*  参数: dataLen ：节点数据长度
*  返回: 队列
****************************************************************************//********************
*/
static pTREE create(const char *name,const char *malloc,uint32_t dataLen)
{
	//确认参数
	if(!name || !malloc || !dataLen)
		return NULL;
	
	//动态内存
	pMALLOC_OPS mem = Tmalloc.find(malloc);									//查找动态内存
	pMALLOC_OPS var = Tmalloc.find("MALLOC_VAR");							//查找动态内存
	
	if(!mem || !var)														//动态内存无效
		return NULL;														//返回空
	
	//申请内存
	pTREE object = var->malloc(sizeof(TREE));								//申请类内存
	PROTECT	*protect = var->malloc(sizeof(PROTECT));						//申请私有变量内存
	pBUFFER stack = Tbuffer.create(name,malloc,sizeof(pTREENODE),NODEMAXNUM);//定义节点堆栈
	if(!object || !protect || !stack)										//内存申请失败
	{
		var->free(object);													//释放对象头
		var->free(protect);													//释放私有变量	
		Tbuffer.destory(stack);												//销毁堆栈
		return NULL;														//返回空
	}
	
	//初始变量
	memset(object,0,sizeof(TREE));
	memset(protect,0,sizeof(PROTECT));					
	//protect
	protect->name = name;													//绑定名称
	protect->dataLen=dataLen;												//数据长度
	protect->stack 		= stack;											//缓存
	//object
	object->mem=mem;														//动态内存
	object->protect = protect;												//赋值保护变量
	object->addNode = addNode;												//绑定函数
	object->delNode = delNode;												//绑定函数
	object->traversal = traversal;											//绑定函数
	object->find_parent = find_parent;										//绑定函数
	object->IsNodeOnTree = IsNodeOnTree;									//绑定函数
	
	//对象链表处理
	pTREE node = firstObj;													//指向首对象
	if(!node)																//无首对象
		firstObj = object;													//赋值首对象
	else																	//之前有对象
	{
		while(node && node->protect)										//匹配所有对象
		{
			protect = node->protect;										//指向私有变量
			if(protect->next)												//本节点非尾节点
				node = protect->next;										//下一节点
			else															//本节点为尾节点
			{
				protect->next = object;										//增加本对象
				break;														//结束匹配
			}
		}
	}
	
	return object;															//返回缓存
}

/*
****************************************************************************//********************
*  函数: 销毁对象函数
*  参数: object ：对象
*  返回: 无
****************************************************************************//********************
*/
static void destory(pTREE object)
{
	PROTECT *protect = object->protect;										//指向私有变量
	//确认参数
	if(!object || !protect)
		return;

	delNode(object,object->rootNode);										//释放树上所有节点
	
	
	//对象链表删除本对象
	PROTECT *pt;
	pTREE node = firstObj;													//指向首对象
	if(node==object)														//本对象为首对象
		firstObj =  protect->next;											//首对象指向本对象下节点
	else																	//本对象非首对象
	{
		while(node && node->protect)										//匹配所有对象
		{
			pt = node->protect;												//指向私有变量
			if(pt->next == object)											//匹配到对象上节点
			{
				pt->next =protect->next;									//对象上节点的下节点 为 对象下节点
				break;														//结束匹配
			}
			node = pt->next;												//指向下一节点
		}
	}
	
	//动态内存
	pMALLOC_OPS var = Tmalloc.find("MALLOC_VAR");							//查找动态内存
	if( !var)																//动态内存无效
		return;																//返回
	
	var->free(object);														//释放对象头
	var->free(protect);														//释放私有变量	
	Tbuffer.destory(protect->stack);										//销毁堆栈
	object->protect = NULL;													//对象无效
}

/*
****************************************************************************//********************
*  函数: 查找树对象
*  参数: name ：对象名称
*  返回: 无
****************************************************************************//********************
*/
static pTREE find(const char *name)
{
	PROTECT *protect;
	pTREE object = firstObj;												//指向首对象
	while(object)															//匹配所有对象
	{
		protect = object->protect;											//指向相应的保护变量
		if(strcmp(name,protect->name)==0)									//匹配到端口名称
			return object;
		object = protect->next;												//指向下一对象
	}
	return NULL;	
}

//end of file.
