/*
 * pocotol.h
 *
 *  Created on: 2021年1月16日
 *      Author: MCX
 */

#ifndef _POTOCOL_H_
#define _POTOCOL_H_

#ifdef __cplusplus
extern "C"{
#endif

#include "maos_include.h"


/*-----------------------------------定义协议应答结构体----------------------------------*/
typedef struct 
{
	uint16_t		sendMaxTime;						//重发次数
	uint16_t		sn;									//序列号
	uint32_t		waitAckMSec;						//等待应答时间 （ms），0：不需要等待应答
	void 			*arg;								//应答回调参数
	void 			(*back)(void *arg,bool ack);		//应答回调函数
	
}POTOCOLACK;

/*--------------------------------定义封装数据包结构体----------------------------------*/
typedef struct 
{
	POTOCOLACK		ack;								//协议应答参数
	uint16_t		dataLen;							//数据长度
	void			*data;								//数据
}PACKDATA;

/*--------------------------------宏定义--------------------------------------------------*/
#define 	PACKAGESIZE(x)		(sizeof(PACKDATA) - sizeof(void*) + x)		//协议封装数据包长度
#define 	PACKAHEADERSIZE		(sizeof(PACKDATA) - sizeof(void*))			//协议封装数据包头长度


/*--------------------------------定义协议结构体----------------------------------*/
typedef struct
{
	bool disable;
	PACKDATA* (*pack_data)(void *buf,void *data,uint16_t len);
	void 	 (*analyze)(void*arg,void *data,uint16_t len);
	bool 	 (*frame_handle)(void*arg,void *data,uint16_t len);

}POCOTOLFUN;


extern POCOTOLFUN usb_vcp,syctp,prol_can2,prol_printf;
PACKDATA* vcp_pack(void *buf,void *data,uint16_t len);


typedef struct 					//CAN外设数据 ID
{
	uint8_t		res:3;				//	00..02 无效位
	uint8_t		msgIndex:5;			//	03..07 消息编号
	uint8_t		taddr:8;			//	08..15 发送者地址
	uint8_t		raddr:8;			//	16..23 接收方地址
	uint8_t		rtype:4;			//	24..27 接收方设备类型
	uint8_t		ttype:4;			//	28..31 发送方设备类型
	
}CANID;

void can_updata_init(void);
#ifdef __cplusplus
}
#endif
#endif /* _POTOCOL_H_ */
