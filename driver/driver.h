/*
 * drivdr.h
 *
 *  Created on: 2021年1月25日
 *      Author: MCX
 */

#ifndef DRIVER_H_
#define DRIVER_H_

#ifdef __cplusplus
extern "C"{
#endif

#include "maos_include.h"

//------------------定义驱动类型枚举体------------------//
typedef enum
{
	DEVICE_TYPE_CHAR = 0,			// 字符设备 
	DEVICE_TYPE_BLOCK,				// 块设备
	DEVICE_TYPE_NETIF,				// 网络接口设备

	DEVICE_TYPE_MAXNUM,
}DEVICE_TYPE;


//-----------------------------定义字符设备操作集结构体--------------------------------//
typedef struct 
{
	bool		(*pfn_open)(void *arg);										//打开设备对象
	void		(*pfn_close)(void );										//关闭设备对象
	bool 		(*pfn_write)(void *data,uint16_t len);						//发送函数
	uint16_t 	(*pfn_read)(void *data);									//接收函数
	void		(*pfn_loop)(void);
}CHAR_DEVICE_OPS;

//--------------------------------定义块设备操作集结构体--------------------------------//
typedef struct 
{
	bool		(*pfn_saveFile)(char *path,char *name,u8 *data,u32 size,void(*pfn_prog)(u8));	//保存文件
	uint32_t	(*pfn_downFile)(char *path,char *name,u8 *data,void(*pfn_prog)(u8));			//下载文件
	bool 		(*pfn_deleFile)(char *name);													//删除文件
	bool 		(*pfn_deleFolder)(char *name);													//删除文件夹
	uint32_t	(*pfn_getFileSize)(char *name);													//获取文件大小
	uint32_t	(*pfn_readFolder)(char *path,void(*back)(char*,char*,u32));						//读文件夹
}BLOCK_DEVICE_OPS;

//--------------------------------定义网络设备操作集结构体-------------------------------//
typedef struct 
{
	bool		(*pfn_open)(void *arg);										//打开设备对象
	void		(*pfn_close)(void );										//关闭设备对象
	bool		(*pfn_connect)(const char *name,uint16_t port,void*client);		//连接服务器
	void		(*pfn_disconnect)(void);									//断开服务器
	bool 		(*pfn_write)(void *data,uint16_t len);						//发送函数
	uint16_t 	(*pfn_read)(void *data);									//接收函数
	bool		(*pfn_getReady)(void);
}NETIF_DEVICE_OPS;

//-----------------------------------定义设备操作集联合体----------------------------------//
typedef union 
{
	CHAR_DEVICE_OPS		*charDevice;
	BLOCK_DEVICE_OPS	*blockDevice;
	NETIF_DEVICE_OPS	*netifDevice;
}DRIVER_OPS;

//-----------------------------------定义设备驱动集结构体----------------------------------//
typedef struct 
{
	const char			*name;							//驱动名称
	DEVICE_TYPE 		type;							//驱动类型
	DRIVER_OPS 			ops;							//设备操作集
}DRIVER;

//-----------------------------------定义设备驱动寄存器集结构体-----------------------------//
typedef struct 
{
	DRIVER				*driver;						//驱动
	DRIVER_INIT			init;							//驱动入口函数
	DRIVER_EXIT			exit;							//驱动出口函数
}DRIVERREG;

//-----------------------------------定义设备驱动寄存器-------------------------------------//
#define DEIVER_RIGISTER(_name,_driver,_init,_exit)	\
		DRIVERREG	driver_##_name __attribute__((section("driveTable"))) =  \
		{\
			.driver = _driver,\
			.init = _init,\
			.exit = _exit\
		}

		
//------------------定义类功能结构体------------------//
typedef struct
{
	/*
	**************************************************
	*  函数: 注册设备
	*  参数: name 设备名称
	*  返回: 设备
	**************************************************
	*/
	DRIVER* (*rigister)(const char *name);

	
	/*
	**************************************************
	*  函数: 注销设备
	*  参数: device 设备
	*  返回: 无
	**************************************************
	*/
	void (*unregister)(const char *name);

	/*
	**************************************************
	*  函数: 查找设备
	*  参数: 参数: name 驱动名称
	*  返回: 设备
	**************************************************
	*/
	DRIVER* (*find)(const char *name);

}TDRIVERFUN;


extern TDRIVERFUN  Tdriver;					//驱动功能函数

void driver_init(void);						//驱动初始化函数

                 	

#ifdef __cplusplus
}
#endif
#endif /* DRIVER_H_ */
