/*
 * driver.c
 *
 *  Created on: 2021年1月25日
 *      Author: MCX
 */

#include "driver.h"
#include "maos_malloc.h"
#include "maos_task.h"

#define	QUEUENAME	"driverQueue"

//--------------------------驱动节点结构体------------------------------//
typedef struct _driverNode 
{
	struct 	_driverNode 	*next;			//下一驱动节点
	DRIVER					*driver;		//驱动对象	
}DRIVERNODE;

//----------------------声名函数----------------------------------------//
static DRIVER* rigister(const char *name);
static DRIVER* find(const char *name);
static void unregister(const char *name);



//----------------------建立变量----------------------------------------//
TDRIVERFUN  Tdriver=
{
	.rigister = rigister,
	.unregister = unregister,
	.find = find,
};

static DRIVERNODE *firsNode=NULL;											//首个驱动对象
extern const unsigned int driveTable$$Base;									//驱动表首
extern const unsigned int driveTable$$Limit;								//驱动表表   


/*
****************************************************************************//********************
*  函数: 注册设备
*  参数: name 驱动名称
*  参数: type 驱动类型
*  参数: ops 操作集
*  返回: 驱动
****************************************************************************//********************
*/
static DRIVER* rigister(const char *name)
{
	//防重处理
	DRIVER *driver = find(name);
	if(driver)
		return driver;
	
	//从驱动表里查找驱动
	DRIVERREG *end=(DRIVERREG*)&driveTable$$Limit;							//指向表首
	DRIVERREG *reg= (DRIVERREG*)&driveTable$$Base;							//指向表尾
	
	while((u32)reg<(u32)end)												//历遍所有驱动表
	{
		if(strcmp(reg->driver->name,name)==0)								//驱动名称正确
			break;															//结束匹配
		reg++;																//下一驱动 
	}
	if((u32)reg>=(u32)end)													//未找到驱动
		return NULL;														//返回
	
	//驱动节点处理
	pMALLOC_OPS var = Tmalloc.find("MALLOC_VAR");							//查找动态内存
	if(!var)																//无动态内存
		return NULL;														//返回
	DRIVERNODE *driverNode = var->malloc(sizeof(DRIVERNODE));				//申请对象公共变量内存
	if( !driverNode)														//内存申请失败
		return NULL;														//返回
	memset(driverNode,0,sizeof(DRIVERNODE));								//初始化节点
	driverNode->driver = reg->driver;										//绑定驱动
	
	//驱动链表处理
	DRIVERNODE *node = firsNode;											//指向首节点
	if(!node)																//无首节点
		firsNode = driverNode;												//赋值首节点
	else																	//之前有节点
	{
		while(node)															//匹配所有节点
		{
			if(node->next)													//本节点非尾节点
				node = node->next;											//下一节点
			else															//本节点为尾节点
			{
				node->next = driverNode;									//增加本对象
				break;														//结束匹配
			}
		}
	}
	
	if(reg->init)															//驱动需要初始化	
		reg->init();														//初始化驱动 

	return reg->driver;														//返回驱动
}

/*
****************************************************************************//********************
*  函数: 注销设备
*  参数: device 设备
*  返回: 无
****************************************************************************//********************
*/
void unregister(const char *name)
{
	//从驱动表里查找驱动
	DRIVERREG *end=(DRIVERREG*)&driveTable$$Limit;							//指向表首
	DRIVERREG *reg= (DRIVERREG*)&driveTable$$Base;							//指向表尾
	
	while((u32)reg<(u32)end)												//历遍所有驱动表
	{
		if(strcmp(reg->driver->name,name)==0)								//驱动名称正确
			break;															//结束匹配
		reg++;																//下一驱动 
	}
	if((u32)reg>=(u32)end)													//未找到驱动
		return ;															//返回
	
	pMALLOC_OPS var = Tmalloc.find("MALLOC_VAR");							//查找动态内存
	if(!var)																//无动态内存
		return ;															//返回
	
	//对象链表删除本设备
	DRIVERNODE *next,*node = firsNode;										//指向首节点
	if(node->driver==reg->driver)											//本节点为首节点
	{
		firsNode =  node->next;												//首节点指向本节点下一节点
		var->free(node);													//释放节点
	}
	else																	//本节点非首对象
	{
		while(node)															//匹配所有节点
		{
			next = node->next;												//指向下一节点
			if(next->driver == reg->driver)									//下一节点为匹配驱动
			{
				node->next =next->next;										//本节点下一节点 为 下一节点的下一节点
				var->free(next);											//释放节点内存
				break;														//结束匹配
			}
			node = next;													//指向下一节点
		}
	}
	
	//卸载驱动
	if(reg->exit)															//驱动需要退出
		reg->exit();														//执行退出

}

/*
****************************************************************************//********************
*  函数: 匹配设备函数
*  参数: name 设备名称
*  返回: 设备
****************************************************************************//********************
*/
DRIVER* find(const char *name)
{
	DRIVER *driver;															//定义驱动
	
	//参数确认
	if(!name)
		return NULL;	

	//匹配设备链表
	DRIVERNODE *node = firsNode;											//指向首驱动节点
	while(node)																//匹配所有节点
	{
		driver = node->driver;												//指向驱动
		if(0==strcmp(name,driver->name))									//匹配到设备名称
			return driver;													//返回驱动
		node = node->next;													//下一驱动节点
	}

	return NULL;															//返回匹配失败
}

/*
****************************************************************************//********************
*  函数: 周期执行函数
*  参数: 无
*  返回: 无
****************************************************************************//********************
*/
static void loop(void)	
{
	DRIVER	*driver;														//定义驱动
				
	//匹配设备链表
	DRIVERNODE *node = firsNode;											//指向首驱动节点
	while(node)																//匹配所有节点
	{
		driver = node->driver;												//指向驱动
		switch(driver->type)												//判断驱动类型
		{
			case DEVICE_TYPE_CHAR:											//字符设备
			{
				if(driver->ops.charDevice->pfn_loop)						//有周期函数
					driver->ops.charDevice->pfn_loop();						//设备周期执行
			}break;
			
			default:break;													//其它类型驱动
		};
		node = node->next;													//下一驱动节点
	}
}


/*
****************************************************************************//********************
*  函数: 设备初始化函数
*  参数: 无
*  返回: 无
****************************************************************************//********************
*/
void driver_init(void)
{
	Task.pfn_add(loop,TASK_IMPORTANT,0);									//添加驱动任务函数
}


//---------------------------------------------test----------------------------------------------//
/*
static void show_all_driver(void)
{
	//从驱动表里查找驱动
	
	DRIVERREG *end=(DRIVERREG*)&driveTable$$Limit;
	DRIVERREG *reg= (DRIVERREG*)&driveTable$$Base;
	shellPrint(&shell,"驱动列表:\r\n\r\n");
	while((u32)reg<(u32)end)
	{
		shellPrint(&shell,"%s\r\n",reg->driver->name);
		reg++;
	}
	shellPrint(&shell,"\r\n");
	if((u32)reg>=(u32)end)
		return ;	
}
static void show_register_driver(void)
{
	DRIVER	*driver;
		
	shellPrint(&shell,"已注册驱动列表:\r\n\r\n");
	//匹配设备链表
	DRIVERNODE *node = firsNode;							//指向首设备
	while(node)												//匹配所有设备
	{
		driver = node->driver;								//指向驱动
		shellPrint(&shell,"%s\r\n",driver->name);

		node = node->next;
	}
	shellPrint(&shell,"\r\n");
}
SHELL_EXPORT_CMD(SHELL_CMD_PERMISSION(0)|SHELL_CMD_TYPE(SHELL_TYPE_CMD_FUNC), show_all_driver, show_all_driver, NULL );	
SHELL_EXPORT_CMD(SHELL_CMD_PERMISSION(0)|SHELL_CMD_TYPE(SHELL_TYPE_CMD_FUNC), show_register_driver, show_register_driver, NULL );
*/
//end of file.
